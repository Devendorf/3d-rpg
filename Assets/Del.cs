﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class Del : MonoBehaviour {

    public InputField inputField;

    private void Start()
    {
        inputField = this.GetComponent<InputField>();
        inputField.onValueChanged.AddListener(Change);
    }

    public void Change(string letter)
    {
        Regex r = new Regex("^[0-9]*$");
        if (r.IsMatch(letter))
        {
            Debug.Log(letter[letter.Length - 1]);
        }
        else
        {
            Debug.Log(letter);
            Debug.Log(letter.Remove(letter.Length - 1));
            letter= letter.Remove(letter.Length - 1);
        }
    }
}
