﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseSkill: MonoBehaviour  {

    bool learned = false;
    int needLvl = 1;
    int BaseDamage = 10;
    float CD = 5f;
    public float castTime;
    public bool ready = true;
    public  int damageFromPoison = 10;
    public  float poisonTime = 5f;
    bool isPoisoned;
    int FinalDamage;
    Dictionary<BaseStat.BaseStatType, int> depends = new Dictionary<BaseStat.BaseStatType, int>();

    public Coroutine coroutine;

    public virtual void CalculateDamage(CharacterStats stats)
    {
        if (depends.Count > 0)
        {
            foreach (BaseStat.BaseStatType type in depends.Values)
            {
                FinalDamage = BaseDamage + stats.stats.Find(x => x.StatType == type).GetFinalValue() * depends[type];
            }
        }
        else
        {
            depends.Add(BaseStat.BaseStatType.Strength, 5);
            CalculateDamage(stats);
        }
    }

    bool SetPoisonDamage(int damage)
    {
        damageFromPoison = damage;
        if (damageFromPoison >0)
            return true;
        else
            return false;
    }

    public bool isPoison()
    {
        return SetPoisonDamage(10);
    }

    public  int GetDamage(CharacterStats stats, MonoBehaviour instance)
    {
        StartCD();
        ready = false;

        coroutine=instance.StartCoroutine(Example(instance));
        CalculateDamage(stats);
        int value = FinalDamage;
        return value;
    }

    void StartCD()
    {
        castTime = CD;
    }

    public virtual IEnumerator Example(MonoBehaviour instance)
    {
        while (castTime > 0)
        {
            castTime -= 0.1f;
            yield return new WaitForSecondsRealtime(0.1f);
        }
            ready = true;
        instance.StopCoroutine(coroutine);
    }

}
