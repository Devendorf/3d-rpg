﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage {

    public static int CalculateDamage(CharacterStats characterStats)
    {
        int damageToDeal = UnityEngine.Random.Range(characterStats.stats.Find(x => x.StatType == BaseStat.BaseStatType.Min_Damage).GetFinalValue(characterStats), 
            characterStats.stats.Find(x => x.StatType == BaseStat.BaseStatType.Max_Damage).GetFinalValue(characterStats) + 1);
        damageToDeal += CalculateCrit(damageToDeal);
        return damageToDeal;
    }

    private static int CalculateCrit(int damage)
    {
        if (UnityEngine.Random.value <= 0.1f)
        {
            int critDamage = (int)(damage * UnityEngine.Random.Range(.25f, .5f));
            return critDamage;
        }
        return 0;
    }
}
