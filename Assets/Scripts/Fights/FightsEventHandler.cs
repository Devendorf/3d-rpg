﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightsEventHandler : MonoBehaviour {

    public delegate void SkillEventHandler(BaseSkill skill);
    public static event SkillEventHandler OnSkillUse;

    public static void UseSkill(BaseSkill skill)
    {
        OnSkillUse(skill);
    }
}
