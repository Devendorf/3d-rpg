﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill1 : BaseSkill
{ 
    int needLvl = 1;
    int BaseDamage = 10;
    int FinalDamage;
    Dictionary<BaseStat.BaseStatType, int> depends = new Dictionary<BaseStat.BaseStatType, int>();

    public override void CalculateDamage(CharacterStats stats)
    {
        if (depends.Count > 0)
        {
            foreach (BaseStat.BaseStatType type in depends.Keys)
            {
                FinalDamage = BaseDamage + stats.stats.Find(x => x.StatType == type).GetFinalValue() * depends[type];
            }
        }
        else
        {
            depends.Add(BaseStat.BaseStatType.Strength, 5);
            CalculateDamage(stats);
        }
    }
}
