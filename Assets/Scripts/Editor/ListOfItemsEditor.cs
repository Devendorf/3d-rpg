﻿using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;


public class ListOfItemsEditor : EditorWindow
{
    public static List<Item> activeItemsWindow= new List<Item>();
    private static ItemDatabase pref_items;
    static Object itemDatabase;
    [MenuItem("Item Database/Insert")]
    static void Insert()
    {
        GetWindow<ListOfItemsEditor>(true, "Редактор вещей", true);
        itemDatabase = Resources.Load("ItemsDatabase");
        if (itemDatabase != null)
            pref_items = (ItemDatabase)Resources.Load("ItemsDatabase");

    }
    bool load = false;

    private void OnGUI()
    {
        pref_items = EditorGUILayout.ObjectField( new GUIContent("Файл предметов"), pref_items, typeof(ItemDatabase), false) as ItemDatabase;
        if (pref_items!=null)
            load = true;
        else
            load = false;
        if (load)
        {
            GUIStyle style = new GUIStyle();
            style.alignment = TextAnchor.MiddleCenter;

            GUIStyle style2 = new GUIStyle();
            style2.alignment = TextAnchor.MiddleLeft;
            style2.margin = new RectOffset(1, 1, 1, 1);

            EditorGUILayout.LabelField("Список предметов:", style);

            if (pref_items.Items.Count > 0)
             {
                 int i = 1;
                foreach (Item item in pref_items.Items.ToList())
                {
                    bool open = false;
                    GUILayout.Space(1f);
                    GUILayout.BeginHorizontal(GUILayout.MaxWidth(position.width / 3));
                    EditorGUILayout.LabelField(i.ToString() + ")", GUILayout.Width(15));
                    if (GUILayout.Button(item.ItemName))
                    {
                        if (activeItemsWindow.Find(x => x == item) == null)
                        {
                            ItemsEditor newEditor=(ItemsEditor)ScriptableObject.CreateInstance(typeof(ItemsEditor));
                            newEditor.Insert(item);
                            activeItemsWindow.Add(item);
                        }
                    }
                    if (open)
                    {
                        item.ItemName = EditorGUILayout.TextField(item.ItemName);
                    }

                    if (GUILayout.Button("X", GUILayout.Width(20)))
                    {
                        pref_items.Items.Remove(item);
                        continue;
                    }
                    i++;
                    //GUI.TextArea(new Rect(3, 30, position.width / 3, 20), attr.attributeName);
                    GUILayout.EndHorizontal();
                }

                // selected = EditorGUILayout.Popup(selected, abc.ToArray());
             }
             if (GUILayout.Button("Add", GUILayout.Width(70)))
             {
                 pref_items.Items.Add(new Item());
             }

        }
    }

    private void OnDestroy()
    {
        if (pref_items != null)
        {
            foreach (Item item in pref_items.Items.ToList())
            {
                if (item.ItemName == "")
                    pref_items.Items.Remove(item);
            }
        }
        AssetDatabase.Refresh();
        EditorUtility.SetDirty(itemDatabase);
        AssetDatabase.SaveAssets();
    }
}