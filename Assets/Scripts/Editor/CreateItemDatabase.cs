﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;

public class CreateItemDatabase : MonoBehaviour
{

    public static ItemDatabase asset;                                                  //The List of all Items

#if UNITY_EDITOR
    public static ItemDatabase createItemsDatabase()                                    //creates a new ItemDatabase(new instance)
    {
        asset = ScriptableObject.CreateInstance<ItemDatabase>();                       //of the ScriptableObject InventoryItemList

        AssetDatabase.CreateAsset(asset, "Assets/Resources/ItemsDatabase.asset");            //in the Folder Assets/Resources/ItemDatabase.asset
        AssetDatabase.SaveAssets();                                                         //and than saves it there        
        return asset;
    }
#endif

}
