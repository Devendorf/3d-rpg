﻿using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;
using System;

public class ItemsEditor : EditorWindow
{
    //private static AttributeList pref;
    ItemsEditor editor2;
    private  Item item;
    private BaseStat sasd;


    public void Insert(Item _item)
    {
        item = _item;
        //GetWindow<Editor2>(true, item.ItemName, true).minSize=new Vector2(400,400);
        editor2 = (ItemsEditor)EditorWindow.GetWindow(typeof(ItemsEditor));
        editor2.titleContent.text = item.ItemName;
        editor2.Show();
    }
    
    bool load = false;

    public Vector2 scrollPosition = Vector2.zero;

    private void OnGUI()
    {
        editor2.titleContent.text = item.ItemName;
        //pref = EditorGUILayout.ObjectField(new GUIContent("Файл"), pref, typeof(AttributeList), false) as AttributeList;
        //if (pref != null)
            load = true;
       // else
         //   load = false;
        if (load)
        {
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            EditorGUILayout.LabelField("Название");
            EditorGUILayout.LabelField("Описание",GUILayout.Height(50));
            EditorGUILayout.LabelField("Техническое название");
            EditorGUILayout.LabelField("Тип предмета");
            if (item.ItemType == Item.ItemTypes.Consumable)
            {
                EditorGUILayout.LabelField("Тип восстановления");
                EditorGUILayout.LabelField("Количество");
            }
            if (item.ItemType == Item.ItemTypes.Clothing)
                EditorGUILayout.LabelField("Слот");
            else
                EditorGUILayout.LabelField("Максимальное количество в слоте");
            EditorGUILayout.LabelField("Предмет для задания");
            GUILayout.EndVertical();
            GUILayout.BeginVertical();
            item.ItemName= EditorGUILayout.TextField(item.ItemName);
            item.Description = EditorGUILayout.TextArea(item.Description, GUILayout.Height(50));
            item.ObjectSlug = EditorGUILayout.TextField(item.ObjectSlug);
            item.ItemType = (Item.ItemTypes)EditorGUILayout.EnumPopup(item.ItemType);
            if (item.ItemType == Item.ItemTypes.Consumable)
            {
                item.IncreaseType = (Item.IncreaseTypes)EditorGUILayout.EnumPopup(item.IncreaseType);
                item.RestoreCount = EditorGUILayout.IntField(item.RestoreCount);
            }
            if (item.ItemType == Item.ItemTypes.Clothing)
                item.SlotType = (ClothSlotScript.SlotTypes)EditorGUILayout.EnumPopup(item.SlotType);
            else
                item.maxStackCount = EditorGUILayout.IntField(item.maxStackCount);
            item.QuestItem = EditorGUILayout.Toggle(item.QuestItem);
            //item.Stats[0].StatType=(BaseStat.BaseStatType) EditorGUILayout.EnumPopup(item.Stats[0].StatType);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            if (item.ItemType == Item.ItemTypes.Clothing)
            {
                GUILayout.Space(20f);
                EditorGUILayout.LabelField("Параметры");
            }
            if (item.ItemType == Item.ItemTypes.Clothing)
            {
                scrollPosition = GUILayout.BeginScrollView(scrollPosition);
                foreach(BaseStat stat in item.Stats)
                {
                    EditorGUILayout.LabelField(stat.StatType.ToString(), GUILayout.Width(Screen.width / 3));
                    stat.BaseValue = EditorGUILayout.IntField(stat.BaseValue);
                }
                GUILayout.EndScrollView();
            }
        }
        else
        {
            EditorGUILayout.HelpBox("Отсутсвует файл с атрибутами. Редактирование вещвей не доступно", MessageType.Error);
        }
    }

    private void OnDestroy()
    {
        if (item != null)
        {
            ListOfItemsEditor.activeItemsWindow.Remove(item);
        }
        AssetDatabase.SaveAssets();
    }
}