﻿using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;


public class CreateDatabases : EditorWindow
{
    [MenuItem("Item Database/Create")]
    static void CreateAll()
    {
        Object itemsDatabase = Resources.Load("ItemsDatabase");
        if (itemsDatabase == null)
            CreateItemDatabase.createItemsDatabase();
        Object inputFile = Resources.Load("InputManager");
        if (inputFile == null)
            CreateInputManager.Create();
    }
}