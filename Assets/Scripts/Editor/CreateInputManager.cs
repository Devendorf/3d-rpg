﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;

public class CreateInputManager : MonoBehaviour
{

    public static InputManager asset;                                                  //The List of all Items

#if UNITY_EDITOR
    public static InputManager Create()                                    //creates a new ItemDatabase(new instance)
    {
        //asset = ScriptableObject.CreateInstance<InputManager>();                       //of the ScriptableObject InventoryItemList

        AssetDatabase.CreateAsset(asset, "Assets/Resources/InputManager.asset");            //in the Folder Assets/Resources/InputManager.asset
        AssetDatabase.SaveAssets();                                                         //and than saves it there        
        return asset;
    }
#endif

}
