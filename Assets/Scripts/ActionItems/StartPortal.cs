﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class StartPortal : MonoBehaviour {
    public Vector3 TeleportLocation { get; set; }
    [SerializeField]private string nextLocation;
    private Scene activeScene;
    private GameObject transport;

    private void Start()
    {
        TeleportLocation = new Vector3(transform.position.x + 2f, transform.position.y, transform.position.z);
        SceneManager.sceneLoaded += SetActive;
        transport = GameObject.Find("Personal");
        SceneManager.LoadSceneAsync(nextLocation, LoadSceneMode.Additive);
    }

    private void SetActive(Scene scene, LoadSceneMode sceneMode)
    {
        SceneManager.MoveGameObjectToScene(transport, scene);
        activeScene = SceneManager.GetActiveScene();
        SceneManager.SetActiveScene(scene);
        SceneManager.sceneLoaded -= SetActive;
        SceneManager.UnloadSceneAsync(activeScene);
        QuestsEventHandler.CheckReadiness();
    }
}
