﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats{
    public List<BaseStat> stats = new List<BaseStat>();

    public CharacterStats(int strength, int agility, int endurance, int armor, int min_damage, int max_damage)
    {
        stats = new List<BaseStat>()
        {
            new BaseStat(BaseStat.BaseStatType.Strength, strength),
            new BaseStat(BaseStat.BaseStatType.Agility, agility),
            new BaseStat(BaseStat.BaseStatType.Endurance, endurance),
            new BaseStat(BaseStat.BaseStatType.Min_Damage, min_damage, BaseStat.BaseStatType.Strength, 1),
            new BaseStat(BaseStat.BaseStatType.Max_Damage, max_damage, BaseStat.BaseStatType.Strength, 1),
            new BaseStat(BaseStat.BaseStatType.Armor, armor)
        };
    }

    public CharacterStats()
    {

    }

    public BaseStat GetStat(BaseStat.BaseStatType stat)
    {
        return this.stats.Find(x => x.StatType == stat);
    }

    public void AddStatBonus(List<BaseStat> statBonuses)
    {
        foreach (BaseStat statBonus in statBonuses) 
        {
            GetStat(statBonus.StatType).AddStatBonus(new StatBonus(statBonus.BaseValue));
        }
    }

    public void RemoveStatBonus(List<BaseStat> statBonuses)
    {
        foreach (BaseStat statBonus in statBonuses)
        {
            GetStat(statBonus.StatType).RemoveStatBonus(new StatBonus(statBonus.BaseValue));
        }
    }

    public void RemoveStatBonus(List<BaseStat> statBonuses, bool needHealthUpdate)
    {
        foreach (BaseStat statBonus in statBonuses)
        {
            GetStat(statBonus.StatType).RemoveStatBonus(new StatBonus(statBonus.BaseValue), needHealthUpdate);
        }
    }
}
