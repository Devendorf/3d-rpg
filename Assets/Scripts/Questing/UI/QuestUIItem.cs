﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestUIItem : MonoBehaviour {

    public Quest quest;
    public Text itemText;
    public Image itemImage;

    public void SetQuest(Quest quest)
    {
        this.quest = quest;
        SetupQuestValues();
    }

    void SetupQuestValues()
    {
        this.gameObject.name = quest.QuestName;
        this.transform.Find("Quest_Name").GetComponent<Text>().text = quest.QuestName;
        UpdateQuest();
    }

    public void UpdateQuest()
    {
        if (quest.Completed)
        {
            this.transform.Find("Complete_Icon").gameObject.SetActive(true);
            this.transform.Find("Complete_Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/Icons/Complete");
        }
        else
            this.transform.Find("Complete_Icon").gameObject.SetActive(false);
    }

    public void UpdateQuest(Quest quest, Transform transform)
    {
        if (quest.Completed)
        {
            transform.Find("Complete_Icon").gameObject.SetActive(true);
            transform.Find("Complete_Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/Icons/Complete");
        }
        else
            transform.Find("Complete_Icon").gameObject.SetActive(false);
    }

    public void Delete(Transform transform)
    {
        if (transform!=null)
            Destroy(transform.gameObject);
    }

    public void OnSelectQuestButton()
    {
        QuestUI.Instance.SetQuestDetails(quest);
    }
}
