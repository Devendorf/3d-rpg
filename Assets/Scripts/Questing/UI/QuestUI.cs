﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestUI : MonoBehaviour {
    public static QuestUI Instance { get; set; }
    public QuestUIDetails QuestUIDetails;
    public RectTransform questPanel;
    public RectTransform scrollViewContent;
    [SerializeField] private InputManager inputManager;
    QuestUIItem QuestContainer { get; set; }
    Item CurrentSelectedItem { get; set; }

    // Use this for initialization
    void Start()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            Instance = this;
        QuestContainer = Resources.Load<QuestUIItem>("UI/Journal/Quest_Container");
        QuestsEventHandler.OnQuestAdded += QuestAdded;
        QuestsEventHandler.OnQuestComplete += QuestChanged;
        QuestsEventHandler.OnQuestEnd += QuestDelete;
        questPanel.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(inputManager.QuestsKeyKode))
        {
            questPanel.gameObject.SetActive(!questPanel.gameObject.activeSelf);
        }
    }

    public void QuestAdded(Quest quest)
    {
        QuestUIItem emptyItem = Instantiate(QuestContainer);
        emptyItem.SetQuest(quest);
        emptyItem.transform.SetParent(scrollViewContent);
    }

    public void QuestChanged(Quest quest)
    {
        if (quest != null)
        {
            QuestUIItem emptyItem = Instantiate(QuestContainer);
            emptyItem.UpdateQuest(quest, scrollViewContent.Find(quest.QuestName));
        }
    }

    public void QuestDelete(Quest quest)
    {
        QuestUIItem emptyItem = Instantiate(QuestContainer);
        emptyItem.Delete(scrollViewContent.Find(quest.QuestName));
        if (QuestUIDetails.Instance!=null)
            QuestUIDetails.Instance.gameObject.SetActive(false);
    }

    public void SetQuestDetails(Quest quest)
    {
        QuestUIDetails.SetQuest(quest);
    }
}
