﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestUIDetails : MonoBehaviour {
    public static QuestUIDetails Instance { get; set; }
    Quest quest;
    Button  questInteractButton;
    Text questNameText, questDescriptionText, questInteractButtonText;
    public Text goalText;

    private void Start()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            Instance = this;
        questNameText = transform.Find("Quest_Name").GetComponent<Text>();
        questDescriptionText = transform.Find("Quest_Description").GetComponent<Text>();
        questInteractButton = transform.Find("Button").GetComponent<Button>();
        questInteractButtonText = questInteractButton.transform.Find("Text").GetComponent<Text>();
        gameObject.SetActive(false);
        UIEventHandler.OnGoalChanged += UpdateGoals;
    }

    public void SetQuest(Quest quest)
    {
        gameObject.SetActive(true);
        goalText.text = "";
        if (quest.Goals != null)
        {
            foreach (Goal goal in quest.Goals)
            {
                goalText.text += goal.Description + ": " + goal.CurrentAmount +@"\"+goal.RequiredAmount+ "\n";
            }
        }
        questInteractButton.onClick.RemoveAllListeners();
        this.quest = quest;
        questNameText.text = quest.QuestName;
        questDescriptionText.text = quest.Description;
        questInteractButtonText.text = "Отказаться";
        questInteractButton.onClick.AddListener(CancelQuest);
    }

    public void CancelQuest()
    {
        Destroy(quest);
        QuestsEventHandler.QuestDelete(quest);
    }

    public void UpdateGoals()
    {
        if (this.gameObject != null && this.gameObject.activeSelf)
        {
            goalText.text = "";
            if (quest.Goals != null)
            {
                foreach (Goal goal in quest.Goals)
                {
                    goalText.text += goal.Description + ": " + goal.CurrentAmount + @"\" + goal.RequiredAmount + "\n";
                }
            }
        }
    }
}
