﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class QuestGiver : NPC {
    public bool AssignedQuest { get; set; }
    public bool Helped { get; set; }
    public GameObject quests;
    public string questType;
    public List<Quest> listOfQuests;
    public Quest Quest { get; set; }
    public Player player;
    public string[] startSize;
    public Info_Panel reward_Panel;
    [HideInInspector] public UnityAction action;
    public bool ReadyToTake;
    public GameObject ReadyObject;
    public GameObject TakeObject;

    GameObject znak;

    private void Awake()
    {
        ReadyToTake = false;
        player = FindObjectOfType<Player>();
        quests = GameObject.Find("Quests");
        FindQuest();
        startSize = dialogue;
        QuestsEventHandler.OnCheck += QuestsReadiness;
    }

    private void OnDestroy()
    {
        QuestsEventHandler.OnCheck -= QuestsReadiness;
    }

    void FindQuest()
    {
        if (quests.GetComponent<CompletedQuests>().quests.Find(x => x == questType) != null)
        {
            AssignedQuest = true;
            Helped = true;
        }
        else
        {
            if (quests.GetComponent(System.Type.GetType(questType)))
            {
                Helped = false;
                AssignedQuest = true;
            }
            else
            {
                Helped = false;
                AssignedQuest = false;
            }
        }
    }

    public override void Interact()
    {
       FindQuest();
        if (!AssignedQuest && !Helped)
        {
            base.Interact();
            AssignQuest();
        }
        else if (AssignedQuest && !Helped)
        {
            CheckQuest();
        }
        else
        {
            DialogueSystem.Instance.AddNewDialogue(startSize, name);
            base.Interact();
        }
    }

   void AssignQuest()
    {
        Quest = (Quest)quests.AddComponent(System.Type.GetType(questType));
        if (player.PlayerLevel.Level >= Quest.LvlNeeded)
        {
            DialogueSystem.Instance.AddNewDialogue(new string[] { "Hello. Do u like to kill some little monsters?", "There's some of them here.", "Go! Kill them!" }, name);
            AssignedQuest = true;
            ReadyToTake = false;
            Quest.Initiate();
            Destroy(znak);
            znak = null;
            SetupIcon();
        }
        else
        { 
            Destroy(quests.GetComponent(System.Type.GetType(questType)));
        }
        
    }

    void CheckQuest()
    {
        Quest = (Quest)quests.GetComponent(System.Type.GetType(questType));
        if (Quest.Completed)
        {
            
            Helped = true;
            AssignedQuest = false;
            DialogueSystem.Instance.AddNewDialogue(new string[] {"Thanks for that!" },name);
            quests.GetComponent<CompletedQuests>().quests.Add(questType);
            Destroy(quests.GetComponent(System.Type.GetType(questType)));
            QuestsEventHandler.QuestDelete(Quest);
            action = () => ShowReward(Quest);
            DialogueSystem.Instance.continueButton.onClick.AddListener(action);
        }
        else
        {
            DialogueSystem.Instance.AddNewDialogue(new string[] { "It's not over!!!!!!!!!" }, name);
        }
    }

    void ShowReward(Quest Quest)
    {
        Quest.GiveReward(player, reward_Panel);
        DialogueSystem.Instance.continueButton.onClick.RemoveListener(action);
    }

    void QuestsReadiness()
    {
        Quest = (Quest)quests.AddComponent(System.Type.GetType(questType));
        if (player.PlayerLevel.Level >= Quest.LvlNeeded && !AssignedQuest)
        {
            ReadyToTake = true;
        }
        SetupIcon();
        Destroy(quests.GetComponent(System.Type.GetType(questType)));
    }


    private void SetupIcon()
    {
        if (ReadyToTake)
        {
            if (znak == null)
            {
                znak = Instantiate(ReadyObject, new Vector3(this.transform.position.x, this.transform.position.y + 2, this.transform.position.z), this.transform.rotation, this.transform);
                ReadyToTake = false;
            }
        }
        else if (AssignedQuest)
        {
            if (znak == null)
            {
                znak = Instantiate(TakeObject, new Vector3(this.transform.position.x, this.transform.position.y + 2, this.transform.position.z), this.transform.rotation, this.transform);
            }
        }
    }
}

