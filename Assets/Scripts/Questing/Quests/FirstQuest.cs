﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FirstQuest : Quest {

	// Use this for initialization
	void Awake () {
        QuestName = "Test Quest";
        Description = "This old man ask me to kill slimes";
        ItemsReward = new List<Item>();
        ItemsReward.Add(InventoryController.Instance.itemDatabase.GetItem("potion_log"));
        ExperienceReward = 7900;
        //LvlNeeded = 2;
        Goals = new List<Goal>();
       // Goals.Add(new KillGoal(this, 0, "Kill 3 slimes", false, 0, 3));
        Goals.Add(new KillGoal(this, 1, "Kill 2 vampires", false, 0, 2));
        //Goals.Add(new CollectionGoal(this, "potion_log", "Get 1 potion log", false, 0, 1));             
    }

    public override void Initiate()
    {
        PortalToAnyScene.Instance.SetSceneAsHome(SceneManager.GetActiveScene());
        Goals.ForEach(g => g.Init());
        QuestsEventHandler.QuestAdded(this);
    }
}
