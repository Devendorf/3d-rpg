﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestsEventHandler : MonoBehaviour {

    public delegate void QuestEventHandler(Quest quest);
    public static event QuestEventHandler OnQuestAdded;
    public static event QuestEventHandler OnQuestComplete;
    public static event QuestEventHandler OnQuestEnd;

    public delegate void QuestGiversCheckHandler();
    public static event QuestGiversCheckHandler OnCheck;
    
    public static void QuestComplete(Quest quest)
    {
        OnQuestComplete(quest);
        //CheckReadiness();
    }

    public static void QuestDelete(Quest quest)
    {
        OnQuestEnd(quest);
    }

    public static void QuestAdded(Quest quest)
    {
        OnQuestAdded(quest);
    }
    
    public static void CheckReadiness()
    {
        OnCheck();
    }
}
