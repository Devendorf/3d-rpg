﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Quest : MonoBehaviour {
    public List<Goal> Goals { get; set; }
    public string QuestName{ get; set; }
    public string Description { get; set; }
    public int ExperienceReward{ get; set; }
    public List<Item> ItemsReward{ get; set; }
    public bool Completed{ get; set; }
    public int LvlNeeded { get; set; }

    public virtual void Initiate()
    { }

        private void Awake()
    {
        //Goals = new List<Goal>();
        //ItemsReward = new List<Item>();
    }

    private void OnDestroy()
    {
        foreach(Goal goal in Goals)
        {
            goal.ClearEvents();
        }
    }

    public void CheckGoals()
    {
        if (Goals != null)
        {
            Completed = Goals.All(g => g.Completed);
            if (Completed)
                QuestsEventHandler.QuestComplete(this);
        }
    }

    public void GiveReward(Player player, Info_Panel reward_Panel)
    {
        Info_Panel Info_PanelInstance = Instantiate(reward_Panel, FindObjectOfType<Canvas>().transform);
        Info_PanelInstance.gameObject.SetActive(true);
        Info_PanelInstance.acceptBtn.onClick.AddListener(() => OnAcceptAll(Info_PanelInstance, player));
        foreach (Item item in ItemsReward)
        {
            DropContainerUI q = Instantiate(Info_PanelInstance.dropContainer);
            q.DropInfoPanel = Info_PanelInstance.DropInfoPanel;
            q.transform.SetParent(Info_PanelInstance.content);
            q.transform.Find("Drop_Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/Icons/Items/" + item.ObjectSlug);
            q.transform.Find("Drop_Name").GetComponent<Text>().text = item.ItemName;
            q.item = item;
            q.GetComponent<Button>().onClick.AddListener(() => OnAccept(q, Info_PanelInstance));
        }
        Info_PanelInstance.text.text = "Получено: \n Опыта: " + ExperienceReward;
    }

    void OnAcceptAll(Info_Panel panel, Player player)
    {
        Destroy(panel.gameObject);
        foreach (Item item in ItemsReward)
        {
            InventoryController.Instance.GiveItem(item);
        }
        player.PlayerLevel.GrantExperience(ExperienceReward);
        //PlayerEventHandler.PlayerLeveled();
    }

    void OnAccept(DropContainerUI container, Info_Panel panel)
    {
        Destroy(container.gameObject);
        if (container.DropInfoPanel.gameObject.activeSelf)
            container.DropInfoPanel.gameObject.SetActive(!container.DropInfoPanel.gameObject.activeSelf);
        InventoryController.Instance.GiveItem(container.item);
        ItemsReward.Remove(container.item);
    }
}
