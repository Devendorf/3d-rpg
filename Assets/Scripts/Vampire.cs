﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class Vampire : BaseEnemy
{ 
    override public void Start()
    {
        DropTable = new DropTable();
        DropTable.loot = new List<LootDrop>
        {
            new LootDrop("potion_log",25),
            new LootDrop("testhead",5)
        };
        ID = 1;
        Experience = 30;
        characterStats = new CharacterStats(30, 10, 200, 2, 1, 2);
        base.Start();
    }
}
