﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropContainerUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Item item;
    public InventoryUIDetails DropInfoPanel;
    public Transform Drop_Icon;
    private bool test, test_1, test_2;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerEnter.transform == Drop_Icon)
        {
            SetTest();
            test = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        test_1 = true;
        DropInfoPanel.gameObject.SetActive(false);
    }

    void SetTest()
    {
        test = false;
        test_1 = false;
        test_2 = false;
        DropInfoPanel.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (test && !test_1 && !test_2 && item != null)
        {
            DropInfoPanel.SetItem(item, GetComponent<Button>());
            test_2 = true;
        }
    }
}
