﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
        UIEventHandler.OnPointsUpdate += Show;
        this.gameObject.SetActive(false);
	}
	
	void Show(int countOfPoints)
    {
        if (countOfPoints > 0)
            this.gameObject.SetActive(true);
        else
            this.gameObject.SetActive(false);
    }
}
