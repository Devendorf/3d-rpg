﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeStats : MonoBehaviour {

    public Text freePointsText;
    private Player player;
    private int startCountOfFreePoints;
    public CharacterStats characterStats= new CharacterStats();
    //private string baseText;

    private List<StatChange> playerStats = new List<StatChange>();
    [SerializeField] private Transform playerStatPanel;
    [SerializeField] private Transform preafabContainer;


    private void Start()
    {
        player = GetComponentInParent<CharacterPanel>().player;
        InitializeStats();
        this.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        if (player != null)
        {
            startCountOfFreePoints = player.freeStatsPoints;
            freePointsText.text = player.freeStatsPoints.ToString();
            UpdateStats();
        }
        
    }

    void InitializeStats()
    {
        for (int i = 0; i < 3; i++)
        {
            playerStats.Add(Instantiate(preafabContainer, playerStatPanel).gameObject.GetComponent<StatChange>());
            playerStats[i].player =player;
            playerStats[i].characterStats = characterStats;
            playerStats[i].freePointsText = freePointsText;
            playerStats[i].indexOfStat = i;
        }
        UpdateStats();
    }

    void UpdateStats()
    {
        if (player.characterStats != null)
        {
            characterStats.stats = new List<BaseStat>();
            foreach (BaseStat stat in player.characterStats.stats)
            {
                characterStats.stats.Add((BaseStat)stat.Clone());
            }
            for (int i = 0; i < 3; i++)
            {

                playerStats[i].statText.text = characterStats.stats[i].StatName + ": ";
                playerStats[i].startCountOfStat= characterStats.stats[i].GetValueWithPoints();
                playerStats[i].statCountText.text =characterStats.stats[i].GetValueWithPoints().ToString();

            }
        }
    }

    public void ConfirmChanges()
    {
        foreach (BaseStat stat in characterStats.stats)
        {
            //Debug.Log(player.characterStats.stats.Find(x => x.StatName == stat.StatName).BaseValue);
            //Debug.Log(stat.BaseValue);
            player.characterStats.stats.Find(x => x.StatName == stat.StatName).PointsToStat = stat.PointsToStat;
            //Debug.Log(player.characterStats.stats.Find(x => x.StatName == stat.StatName).BaseValue);
        }
        startCountOfFreePoints = player.freeStatsPoints;
        PlayerEventHandler.StatsChanged(true);
        UIEventHandler.PointsCountChanged(player.freeStatsPoints);
        if (player.freeStatsPoints > 0)
            UpdateStats();
        else
            this.gameObject.SetActive(false);
    }

    public void ResetChanges()
    {
        player.freeStatsPoints = startCountOfFreePoints;
        freePointsText.text = player.freeStatsPoints.ToString();
        UpdateStats();
    }
}
