﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WorldInteraction : MonoBehaviour {
    NavMeshAgent playerAgent;
    Player player;
    IWeapon equippedItem;
    GameObject goingToThisObject;
        
	// Use this for initialization
	void Start ()
    {
        playerAgent = GetComponent<NavMeshAgent>();
        player = GetComponent<Player>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetMouseButtonDown(0) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            if (!player.busy)
                GetInteraction();
	}

    void GetInteraction()
    {
        Ray interactionRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit InteractionInfo;
        if (Physics.Raycast(interactionRay, out InteractionInfo, Mathf.Infinity))
        {
            if (goingToThisObject != null)
            {
                goingToThisObject.GetComponent<Interactable>().StopInteraction();
                goingToThisObject = null;
            }
            playerAgent.updateRotation = true;
            GameObject interactedObject = InteractionInfo.collider.gameObject;
            if (interactedObject.tag == "Enemy")
            {
                //equippedItem = GetComponent<PlayerWeaponController>().playerHand.transform.GetChild(0).gameObject.GetComponent<IWeapon>();     Для автоатаки
                interactedObject.GetComponent<Interactable>().MoveToInteraction(playerAgent);
                goingToThisObject = interactedObject;
            }
            else if (interactedObject.tag == "Interactable Object")
            {
                interactedObject.GetComponent<Interactable>().MoveToInteraction(playerAgent);
                goingToThisObject = interactedObject;
            }
            else
            {
                playerAgent.stoppingDistance = 0;
                playerAgent.destination = InteractionInfo.point;
            }
        }
    }
}
