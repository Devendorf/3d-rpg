﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLevel : MonoBehaviour {
    public int Level { get; set; }
    public int CurrentExperience { get; set; }
    public int RequiredExperience { get { return (Lvl_exp[Level-1]); } }
    private List<int> Lvl_exp;

    // Use this for initialization
    void Start () {
        CombatEvents.OnEnemyDeath += EnemyToExperience;
        Lvl_exp = new List<int>
        {
            8000,
            19000,
            37000,
            61000
        };
        Level = 1;
        //Исправить порядок
        //UIEventHandler.PlayerLeveled();
    }

    public void EnemyToExperience(IEnemy enemy)
    {
        GrantExperience(enemy.Experience);
    }

    public void GrantExperience(int amount)
    {
        CurrentExperience += amount;
        if(CurrentExperience >= RequiredExperience)
        {
            CurrentExperience -= RequiredExperience;
            Level++;
            PlayerEventHandler.PlayerLevelUp();
        }
        PlayerEventHandler.PlayerLeveled();
    }
}
