﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatChange : MonoBehaviour {

    public Player player;
    public CharacterStats characterStats;
    public int indexOfStat;
    public int startCountOfStat;
    public Text statText;
    public Text statCountText;
    public Text freePointsText;

    public void AddToStat()
    {
        if (player.freeStatsPoints > 0)
        {
            characterStats.stats[indexOfStat].PointsToStat += 1;
            statCountText.text = characterStats.stats[indexOfStat].GetValueWithPoints().ToString();
            player.freeStatsPoints -= 1;
            freePointsText.text = player.freeStatsPoints.ToString();
        }
    }

    public void RemoveFromStat()
    {
        if (characterStats.stats[indexOfStat].GetValueWithPoints() > startCountOfStat)
        {
            characterStats.stats[indexOfStat].PointsToStat -= 1;
            statCountText.text = characterStats.stats[indexOfStat].GetValueWithPoints().ToString();
            player.freeStatsPoints += 1;
            freePointsText.text = player.freeStatsPoints.ToString();
        }
    }
}
