﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Exp_Bar : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
    [SerializeField] private Image experienceFill;
    [SerializeField] private Text experienceValue;
    [SerializeField] private Player player;

    private void Start()
    {
        UpdateExperience();
        experienceValue.gameObject.SetActive(false);
        PlayerEventHandler.OnPlayerLevel += UpdateExperience;
    }

    public void UpdateExperience()
    {
        experienceFill.fillAmount = (float)player.PlayerLevel.CurrentExperience / (float)player.PlayerLevel.RequiredExperience;
        this.experienceValue.text = player.PlayerLevel.CurrentExperience.ToString() + "/" + player.PlayerLevel.RequiredExperience.ToString();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        experienceValue.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        experienceValue.gameObject.SetActive(false);
    }

}
