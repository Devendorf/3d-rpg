﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Info_Panel : MonoBehaviour {
    public Text text;
    public Button acceptBtn;
    public Button declineBtn;
    public RectTransform content;
    public Image dropImage;
    public DropContainerUI dropContainer;
    public InventoryUIDetails DropInfoPanel;
}
