﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyRandom {

    public static bool Get_Result(int random_chance)
    {
        bool value = false;
        bool need_add;
        List<int> have_items = new List<int>();
        while (have_items.Count < random_chance)
        {
            need_add = true;
            int temp_item = Random.Range(0, 101);
            foreach (int item in have_items)
            {
                if (item == temp_item)
                    need_add = false;
            }
            if (need_add)
                have_items.Add(temp_item);
        }
        int rand_item = Random.Range(0, 101);
        foreach (int item in have_items)
        {
            if (item == rand_item)
                value = true;
        }
        return value;
    }

    public static bool Get_Result(double random_chance)
    {
        bool value = false;
        int asd = (int)((random_chance - (int)random_chance) * 100);
        int aaa = (int)random_chance;
        if (Get_Result(asd) && Get_Result(aaa))
            value = true;
        else
            value = false;
        return value;
    }
}
