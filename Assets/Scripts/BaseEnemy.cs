﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BaseEnemy : MonoBehaviour, IEnemy
{
    public LayerMask aggroLayerMask;
    public int currentHealth;
    public int maxHealth;
    public int ID { get; set; }
    private NavMeshAgent navAgent;
    [HideInInspector] public CharacterStats characterStats;
    private Collider[] withinAggroColliders;
    private Player player;

    public int Experience { get; set; }
    public DropTable DropTable { get; set; }
    public Spawner Spawner { get; set; }
    public PickupItem pickUpItem;
    
    bool alive;

    Coroutine coroutine;

    public virtual void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        currentHealth = maxHealth;
        alive = true;
    }

    private void FixedUpdate()
    {
        withinAggroColliders = Physics.OverlapSphere(transform.position, 10, aggroLayerMask);
        if (withinAggroColliders.Length > 0)
        {
            ChasePlayer(withinAggroColliders[0].GetComponent<Player>());
        }
    }

    public void PerformAttack()
    {
        player.TakeDamage(Damage.CalculateDamage(characterStats));
    }

    void ChasePlayer(Player player)
    {
        if (!player.died)
        {
            navAgent.SetDestination(player.transform.position);
            this.player = player;
            if (navAgent.remainingDistance <= navAgent.stoppingDistance)
            {
                if (!IsInvoking("PerformAttack"))
                    InvokeRepeating("PerformAttack", 0.5f, 2f);
            }
            else
            {
                if (IsInvoking("PerformAttack"))
                    CancelInvoke("PerformAttack");
            }
        }
        else
        {
            if (IsInvoking("PerformAttack"))
                CancelInvoke("PerformAttack");
            ReturnToSpawn();
            Debug.Log("He almost dead!");
        }
    }

    void ReturnToSpawn()
    {
        navAgent.SetDestination(Spawner.transform.position);
    }

    public void TakeDamage(int amount)
    {
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public void TakeDamageFromPoison(int amount, float time)
    {
        coroutine=StartCoroutine(DamageTime(amount, time));
    }

    IEnumerator DamageTime(int damage, float time)
    {
        while (time > 0)
        {
            TakeDamage(damage);
            time -= 1f;
            yield return new WaitForSecondsRealtime(1f);
        }
        StopCoroutine(coroutine);
    }

    public void Die()
    {
        if (alive)
        {
            alive = false;
            DropLoot();
            CombatEvents.EnemyDied(this);
            if (Spawner != null)
                this.Spawner.Respawn();
            Destroy(gameObject);
        }
    }

    void DropLoot()
    {
        List<Item> items = DropTable.GetDrop();
        if (items != null)
        {
            PickupItem instance = Instantiate(pickUpItem, transform.position, Quaternion.identity, this.transform.parent);
            instance.ItemsDrop = items;
        }
    }

    IEnumerator IEnemy.DamageTime(int amount, float time)
    {
        throw new NotImplementedException();
    }
}
