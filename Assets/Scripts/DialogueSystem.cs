﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class DialogueSystem : MonoBehaviour {

    public static DialogueSystem Instance { get; set; }
    public GameObject dialogPanel;
    public List<string> dialogueLines = new List<string>();
    public string npcName;
    public Button continueButton;
    Text dialogueText, nameText;
    int dialogueIndex;

    void Awake () {
        continueButton = dialogPanel.transform.Find("Continue").GetComponent<Button>();
        dialogueText = dialogPanel.transform.Find("Text").GetComponent<Text>();
        nameText = dialogPanel.transform.Find("Name").GetChild(0).GetComponent<Text>();
        continueButton.onClick.AddListener(delegate { ContinueDialogue(); });
        dialogPanel.SetActive(false);


        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
	}
	
	public void AddNewDialogue(string[] lines, string npcName) {
        dialogueIndex = 0;
        dialogueLines = new List<string>(lines.Length);
        dialogueLines.AddRange(lines);
        this.npcName = npcName;
        CreateDialogue();
	}


    public void CreateDialogue() {
        WriteMessage();
        nameText.text = npcName;
        dialogPanel.SetActive(true);
    }

    public void ContinueDialogue() {
        if (dialogueIndex < dialogueLines.Count - 1) {
            dialogueIndex++;
            WriteMessage();
        }
        else
        {
            dialogPanel.SetActive(false);
        }
    }

    void WriteMessage()
    {
        dialogueText.text = dialogueLines[dialogueIndex];
    }

    public void AddToButton()
    {
        
    }
}
