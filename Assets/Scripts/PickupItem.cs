﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickupItem : Interactable
{
    public List<Item> ItemsDrop { get; set; }
    public Info_Panel Info_Panel;
    private Info_Panel info_Panel_opened;
    private static bool opened;

    private float spawnTime;

    public override void Start()
    {
        spawnTime = Time.time;
    }

    new void Update()
    {
        if (spawnTime!=0 && Time.time - spawnTime != 0)
        {
            if (Time.time - spawnTime >= 10f)
            {
                if (opened)
                {
                    info_Panel_opened.gameObject.SetActive(false);
                    Destroy(info_Panel_opened);
                    opened = false;
                }
                Destroy(gameObject);
            }
        }
        base.Update();
    }

    public override void Interact()
    {
        if (!opened)
        {
            opened = true;
            Info_Panel Info_PanelInstance = Instantiate(Info_Panel, FindObjectOfType<Canvas>().transform);
            Info_PanelInstance.gameObject.SetActive(true);
            foreach (Item item in ItemsDrop)
            {
                DropContainerUI q = Instantiate(Info_PanelInstance.dropContainer);
                q.DropInfoPanel = Info_PanelInstance.DropInfoPanel;
                q.transform.SetParent(Info_PanelInstance.content);
                q.transform.Find("Drop_Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/Icons/Items/" + item.ObjectSlug);
                q.transform.Find("Drop_Name").GetComponent<Text>().text = item.ItemName;
                q.item = item;
                q.GetComponent<Button>().onClick.AddListener(() => OnAccept(q, Info_PanelInstance));
            }
            Info_PanelInstance.acceptBtn.onClick.AddListener(() => OnAcceptAll(Info_PanelInstance));
            Info_PanelInstance.declineBtn.onClick.AddListener(() => OnDecline(Info_PanelInstance));
            info_Panel_opened = Info_PanelInstance;
        }
    }

    void OnAcceptAll(Info_Panel panel)
    {
        opened = false;
        Destroy(panel.gameObject);
        foreach (Item item in ItemsDrop)
        {
            InventoryController.Instance.GiveItem(item);
        }
        Destroy(gameObject);
    }

    void OnDecline(Info_Panel panel)
    {
        opened = false;
        Destroy(panel.gameObject);
    }

    void OnAccept(DropContainerUI container, Info_Panel panel)
    {
        Destroy(container.gameObject);
        if (container.DropInfoPanel.gameObject.activeSelf)
            container.DropInfoPanel.gameObject.SetActive(!container.DropInfoPanel.gameObject.activeSelf);
        InventoryController.Instance.GiveItem(container.item);
        ItemsDrop.Remove(container.item);
        if (ItemsDrop.Count == 0)
        {
            opened = false;
            Destroy(panel.gameObject);
            Destroy(gameObject);
        }
    }
}
