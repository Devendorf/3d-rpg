﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterPanel : MonoBehaviour {


    [SerializeField] private Text  level;
    [SerializeField] private Image levelFill;
    [SerializeField] public Player player;

    //Stats
    private List<Text> playerStatTexts = new List<Text>();
    [SerializeField] private Text playerStatPrefab;
    [SerializeField] private Transform playerStatPanel;

    private float lastClick;

    // Use this for initialization
    void Start () {

        PlayerEventHandler.OnStatsChanged += UpdateStats;
        PlayerEventHandler.OnPlayerLevel += UpdateLevel;
        InitializeStats();
	}

    public void UpdateLevel()
    {
        this.level.text = player.PlayerLevel.Level.ToString();
        this.levelFill.fillAmount = (float)player.PlayerLevel.CurrentExperience / (float)player.PlayerLevel.RequiredExperience;
    }

    void InitializeStats()
    {
        for (int i = 0; i < player.characterStats.stats.Count-1; i++)
        {
            playerStatTexts.Add(Instantiate(playerStatPrefab));
            playerStatTexts[i].transform.SetParent(playerStatPanel);
        }
        UpdateStats();
    }

    void UpdateStats()
    {
        string damage_str = "Урон: ";
        for (int i = 0; i < player.characterStats.stats.Count; i++)
        {
            if (player.characterStats.stats[i].StatType == BaseStat.BaseStatType.Min_Damage)
                damage_str += player.characterStats.stats[i].GetFinalValue(player.characterStats).ToString() + "-";
            else if (player.characterStats.stats[i].StatType == BaseStat.BaseStatType.Max_Damage)
            {
                damage_str += player.characterStats.stats[i].GetFinalValue(player.characterStats);
            }
            else
            {
                string bonusesStr = "";
                if (player.characterStats.stats[i].GetBonuses() != 0)
                    bonusesStr = "+" + player.characterStats.stats[i].GetBonuses().ToString();
                if (i<3)
                    playerStatTexts[i].text = player.characterStats.stats[i].StatName + ": " + player.characterStats.stats[i].GetValueWithPoints().ToString() + bonusesStr;
                else
                {
                    if (i==5)
                        playerStatTexts[i-1].text = player.characterStats.stats[i].StatName + ": " + player.characterStats.stats[i].GetValueWithPoints().ToString() + bonusesStr;
                }
            }
            playerStatTexts[3].text = damage_str;
        }
    }
}
