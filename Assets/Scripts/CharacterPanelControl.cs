﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterPanelControl : MonoBehaviour {

    public RectTransform characterPanel;
    public CharacterPanel characterPanelscipt;
    [SerializeField] private InputManager inputManager;

    // Use this for initialization
    void Start () {
        characterPanel.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(inputManager.CharacterPanelKeyCode))
        {
            characterPanel.gameObject.SetActive(!characterPanel.gameObject.activeSelf);
            if (characterPanel.gameObject.activeSelf)
            {
               characterPanelscipt.UpdateLevel();
            }
            
        }
    }
}
