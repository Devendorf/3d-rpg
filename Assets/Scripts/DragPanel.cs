﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class DragPanel : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Transform oldParent;
    private Vector3 previous, real;
    private string objectName;

    public void OnBeginDrag(PointerEventData eventData)
    {
        objectName = eventData.pointerCurrentRaycast.gameObject.name;
        previous = Input.mousePosition;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (objectName == this.gameObject.name)
        {
            real = Input.mousePosition - previous;
            transform.position = transform.position + real;
            previous = Input.mousePosition;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
    }
}