﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEventHandler : MonoBehaviour {

    public delegate void PlayerHealthEventHandler();
    public static event PlayerHealthEventHandler OnPlayerHealhChanged;
    public static event PlayerHealthEventHandler OnPlayerHealthRestored;

    public delegate void StatsEventHandler();
    public static event StatsEventHandler OnStatsChanged;

    public delegate void PlayerLevelEventHandler();
    public static event PlayerLevelEventHandler OnPlayerLevel;
    public static event PlayerLevelEventHandler OnPlayerLevelUp;

    public static void HealthChanged()
    {
        OnPlayerHealhChanged();
        OnPlayerHealthRestored();
    }

    public static void StatsChanged(bool needHealthUpd)
    {
        OnStatsChanged();
        if (needHealthUpd)
            HealthChanged();
    }

    public static void PlayerLeveled()
    {
        OnPlayerLevel();
    }

    public static void PlayerLevelUp()
    {
        OnPlayerLevelUp();
        QuestsEventHandler.CheckReadiness();
    }
}
