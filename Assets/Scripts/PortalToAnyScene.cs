﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalToAnyScene : MonoBehaviour {
    public static PortalToAnyScene Instance { get; set; }
    public Vector3 TeleportLocation { get; set; }
    //[SerializeField] private string nextLocation;
    private Scene activeScene;
    //private Scene need;
    public Scene Home;
    public string homeName;
    private GameObject transport;

    private void Start()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            Instance = this;
        TeleportLocation = new Vector3(transform.position.x + 2f, transform.position.y, transform.position.z);
        //need = SceneManager.GetSceneByName(nextLocation);
        //SceneManager.sceneLoaded += SetActive;
    }

    public void TeleportToHome()
    {
        transport = GameObject.Find("Personal");
        SceneManager.sceneLoaded += SetActive;
        SceneManager.LoadSceneAsync(homeName, LoadSceneMode.Additive);
        /*
        player.transform.position = portal.TeleportLocation;
        player.GetComponent<NavMeshAgent>().ResetPath();*/
    }

    public void TeleportToLocation(string locationName)
    {
        transport = GameObject.Find("Personal");
        SceneManager.sceneLoaded += SetActive;
        SceneManager.LoadSceneAsync(locationName, LoadSceneMode.Additive);
    }

    private void SetActive(Scene scene, LoadSceneMode sceneMode)
    {
        SceneManager.MoveGameObjectToScene(transport, scene);
        activeScene = SceneManager.GetActiveScene();
        SceneManager.SetActiveScene(scene);
        SceneManager.sceneLoaded -= SetActive;
        SceneManager.UnloadSceneAsync(activeScene);
        //QuestsEventHandler.CheckReadiness();
    }

    public void SetSceneAsHome(Scene scene)
    {
        this.Home = scene;
        homeName = Home.name;
    }
}
