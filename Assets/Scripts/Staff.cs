﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Staff : MonoBehaviour, IWeapon, IProjectileWeapon
{
    private Animator animator;     
    public List<BaseStat> Stats { get; set; }
    public Item.WeaponTypes WeaponType { get; set; }
    public float AttackDistance { get; set; }
    public Transform ProjectileSpawn{get; set;}
    public int CurrentDamage { get; set; }

    public Fireball fireball;
    void Start()
    {
        fireball = Resources.Load<Fireball>("Weapons/Projectiles/Fireball");
        AttackDistance = 10f;
        animator = GetComponent<Animator>();    
    }

    public void PerformAttack(int damage, bool _isPoison = false)
    {
        animator.SetTrigger("Base_Attack");
    }

    public void PerformSpecialAttack(int damage, int poisdmg, bool _isPoison, float time)
    {
        animator.SetTrigger("Base_Attack");
    }

    public void CastProjectile()
    {
        Fireball fireballInstance = Instantiate(fireball, ProjectileSpawn.position, ProjectileSpawn.rotation);
        fireballInstance.SetRange(AttackDistance);
        //fireballInstance.Damage = CharacterStats.GetStat(BaseStat.BaseStatType.Power).GetCalculatedStatValue();
        fireballInstance.Damage = 15;
        fireballInstance.Direction = ProjectileSpawn.forward;
    }
}
