﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour, IWeapon
{
    private Animator animator;     
    public List<BaseStat> Stats { get; set; }
    public Item.WeaponTypes WeaponType { get; set; }
    public float AttackDistance { get; set; }
    public int CurrentDamage{get;set;}
    private bool isPoison { get; set; }
    private int DamageFromPoison { get; set; }
    private float PoisonTime { get; set; }

    List<Collider> enemies;

    void Start()
    {
        animator = GetComponent<Animator>();    
    }

    public void PerformAttack(int damage, bool _isPoison=false)
    {
        enemies = new List<Collider>();
        CurrentDamage = damage;
        isPoison = _isPoison;
        animator.SetTrigger("Base_Attack");
    }

    public void PerformSpecialAttack(int damage,int poisdmg, bool _isPoison, float time)
    {
        enemies = new List<Collider>();
        CurrentDamage = damage;
        DamageFromPoison = poisdmg;
        isPoison = _isPoison;
        PoisonTime = time;
        animator.SetTrigger("Base_Attack");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag== "Enemy")
        {
            if (!enemies.Find(x => x == other))
            {
                enemies.Add(other);
                other.GetComponent<IEnemy>().TakeDamage(CurrentDamage);
                if (isPoison)
                    other.GetComponent<IEnemy>().TakeDamageFromPoison(DamageFromPoison, PoisonTime);
            }
        }
    }

}
