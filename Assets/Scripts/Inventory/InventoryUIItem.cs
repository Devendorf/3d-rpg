﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryUIItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private RectTransform invPanel;
    public Item item;
    public Text itemText;
    public Image itemImage;
    public Button btn;
    private bool test,test_1, test_2;
    private float lastClick;
    public bool empty;
    public Text visualCount;
    public HotBarSlot hotBarSlot;

    private void Start()
    {
        visualCount.gameObject.SetActive(false);
        this.btn = this.transform.GetComponent<Button>();
        btn.onClick.AddListener(OnEmptySlotClick);
        empty = true;
        invPanel = (RectTransform)gameObject.transform.parent.transform.parent;
    }

    public void SetItem(Item item)
    {
        empty = false;
        this.item = item;
        SetupItemValues();
        UIEventHandler.OnInventoryClosed += SetTest;
    }

    void SetupItemValues()
    {
        this.btn = this.transform.GetComponent<Button>();
        btn.onClick.AddListener(OnItemClick);
        //this.transform.Find("Item_Name").GetComponent<Text>().text = item.ItemName;
        this.transform.Find("Item_Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/Icons/Items/" + item.ObjectSlug);
        UpdateVisualCount();
    }

    public void UpdateVisualCount()
    {
        if (item != null)
        {
            if (item.Count > 1)
            {
                this.visualCount.gameObject.SetActive(true);
                this.visualCount.text = item.Count.ToString();
            }
            else
            {
                this.visualCount.gameObject.SetActive(false);
            }
        }
        else
            this.visualCount.gameObject.SetActive(false);
        if (hotBarSlot != null)
            hotBarSlot.UpdateVisualCount();
    }

    public void SetEmptySlot()
    {
        this.transform.Find("Item_Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/Icons/Items/EmptySlot");
        empty = true;
        item = null;
        UpdateVisualCount();
        btn.onClick.RemoveAllListeners();
        btn = null;
        if (hotBarSlot != null)
        {
            hotBarSlot.SetEmptySlot();
        }
        hotBarSlot = null;
        SetTest();
    }

    public void OnEmptySlotClick()
    {
        invPanel.SetAsLastSibling();
    }

    public void OnItemClick()
    {
        invPanel.SetAsLastSibling();
        if (lastClick != 0 && Time.time - lastClick != 0)
        {
            if (Time.time - lastClick < 0.4f)
            {
                //Debug.Log(this.item.ObjectSlug);
                //UIEventHandler.UseItem();
                OnItemInteract();
            }
        }
        lastClick = Time.time;
    }

    public void OnItemInteract()
    {
        if (item != null && !item.QuestItem && !item.busy)
        {
            if (item.ItemType == Item.ItemTypes.Consumable)
            {
                InventoryController.Instance.ConsumeItem(item);
                item.Count--;
                if (item.Count == 0)
                {
                    UIEventHandler.ClearSlot(GetComponent<Button>().gameObject);
                    item = null;
                }
            }
            else if (item.ItemType == Item.ItemTypes.Clothing)
            {

                InventoryController.Instance.EquipItem(item);
                UIEventHandler.ClearSlot(GetComponent<Button>().gameObject);
                item = null;
            }
            UpdateVisualCount();
            
            //gameObject.SetActive(false);
        }
    }

    /*public void OnSelectItemButton()
    {
        InventoryController.Instance.SetItemDetails(item, GetComponent<Button>());
    }*/

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!empty)
        {
            SetTest();
            //StartCoroutine(Example());
            test = true;
        }
    }

    IEnumerator Example()
    {
        yield return new WaitForSecondsRealtime(1f);
        //test = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        test_1 = true;
        //StopCoroutine(Example());
        InventoryController.Instance.ClosePanelDetails();
    }

    void SetTest()
    {
        test = false;
        test_1 = false;
        test_2 = false;
        InventoryController.Instance.ClosePanelDetails();
    }

    private void Update()
    {
        if (test && !test_1 && !test_2&&item!=null)
        {
            InventoryController.Instance.SetItemDetails(item, GetComponent<Button>());
            test_2 = true;
        }
    }


    //Перетаскивание объекта
    private Transform oldParent;
    private Vector3 previous, real,start;
    private string objectName;

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!empty)
        {
            invPanel.SetAsLastSibling();
            start = itemImage.transform.position;
            oldParent = itemImage.gameObject.transform.parent;
            itemImage.transform.SetParent(FindObjectOfType<Canvas>().transform);
            objectName = eventData.pointerCurrentRaycast.gameObject.name;
            previous = Input.mousePosition;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (objectName == this.gameObject.name&& item!=null&& !empty)
        {
            //this.gameObject.transform.SetAsLastSibling();
            real = Input.mousePosition - previous;
            //transform.position = transform.position + real;
            itemImage.gameObject.transform.position = itemImage.transform.position + real;
            previous = Input.mousePosition;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!empty)
        {
            //Debug.Log(eventData.pointerCurrentRaycast.gameObject.name);
            //if (eventData.pointerCurrentRaycast.gameObject.GetComponent<InventoryUIItem>())
            itemImage.gameObject.transform.SetParent(oldParent);
            itemImage.transform.position = start;
            if (eventData.pointerCurrentRaycast.gameObject == null)
            {
                DropItem();
            }
            else
            if (this != eventData.pointerCurrentRaycast.gameObject.GetComponent<InventoryUIItem>() && eventData.pointerCurrentRaycast.gameObject.GetComponent<InventoryUIItem>() != null)
                ChangeTwoObj(eventData.pointerCurrentRaycast.gameObject.GetComponent<InventoryUIItem>());
            else if(this != eventData.pointerCurrentRaycast.gameObject.GetComponent<InventoryUIItem>() && eventData.pointerCurrentRaycast.gameObject.GetComponent<HotBarSlot>() != null)
            {
                if (item.ItemType == Item.ItemTypes.Consumable)
                {
                    if (hotBarSlot == null)
                    {
                        SetHotBarSlot(eventData.pointerCurrentRaycast.gameObject.GetComponent<HotBarSlot>(), this, true);
                       // hotBarSlot = eventData.pointerCurrentRaycast.gameObject.GetComponent<HotBarSlot>();
                        //hotBarSlot.inventoryUIItem = GetLinkToItem();
                       // hotBarSlot.SetItem();
                    }
                }
            }
            else
            {
                GameObject inv = GameObject.Find("Panel_Inventory");
                if (eventData.pointerCurrentRaycast.gameObject != inv && eventData.pointerCurrentRaycast.gameObject.transform.parent.gameObject != inv)
                    DropItem();
            }
            //else
        }
    }

    void ChangeTwoObj(InventoryUIItem target)
    {
        if (target.item != null && this.item.ObjectSlug == target.item.ObjectSlug)
            StackItems(target);
        else
            SwapItems(target);
        UIEventHandler.SlotRecount(GetComponent<Button>().gameObject);
        UIEventHandler.SlotRecount(target.GetComponent<Button>().gameObject);
    }

    public InventoryUIItem GetLinkToItem()
    {
        return this;
    }

    public void SetHotBarSlot(HotBarSlot hotBarSlot ,InventoryUIItem target, bool need_re)
    {
        if (hotBarSlot.inventoryUIItem != null && need_re)
        {
            hotBarSlot.inventoryUIItem.hotBarSlot = null;
        }
        hotBarSlot.inventoryUIItem = target.GetLinkToItem();
        hotBarSlot.SetItem();
        target.hotBarSlot = hotBarSlot;
    }

    void SwapItems(InventoryUIItem target)
    {
        Item temp = target.item;
        if (!item.busy)
        {
            if (target.empty)
            {
                target.SetItem(this.item);
                if (hotBarSlot != null)
                {
                    SetHotBarSlot(hotBarSlot, target, true);
                    this.hotBarSlot = null;
                }
                SetEmptySlot();
            }
            else
            {
                if (!temp.busy)
                {
                    target.SetItem(this.item);
                    this.SetItem(temp);
                    HotBarSlot tempHotBarSlot = null;
                    /*if (target.hotBarSlot != null || hotBarSlot != null)
                    {
                        if (target.hotBarSlot != null)
                        {
                            tempHotBarSlot = target.hotBarSlot;
                            target.hotBarSlot.inventoryUIItem = GetLinkToItem();
                            target.hotBarSlot.SetItem();
                        }
                        if (tempHotBarSlot != null)
                        {
                            hotBarSlot = tempHotBarSlot;
                            hotBarSlot.inventoryUIItem = target.GetLinkToItem();
                            hotBarSlot.SetItem();
                            if (tempHotBarSlot != null)
                            {
                                
                            }
                            else
                            {
                                hotBarSlot.SetEmptySlot();
                            }
                        }
                    }*/
                    if (target.hotBarSlot != null && hotBarSlot != null)
                    {
                        tempHotBarSlot = target.hotBarSlot;
                        SetHotBarSlot(hotBarSlot, target, false);
                        SetHotBarSlot(tempHotBarSlot, this, false);
                    }
                    else if (target.hotBarSlot != null)
                    {
                        SetHotBarSlot(target.hotBarSlot,this, true);
                    }
                    else if(hotBarSlot != null)
                    {
                        SetHotBarSlot(hotBarSlot, target, true);
                        //this.hotBarSlot = null;
                    }
                }
            }
            
        }
    }

    void StackItems(InventoryUIItem target)
    {
        Item itemTarget = target.item;
        if (itemTarget.Count < itemTarget.maxStackCount)
        {
            if (itemTarget.Count + this.item.Count > itemTarget.maxStackCount)
            {
                int maxAdd;
                maxAdd = itemTarget.maxStackCount - itemTarget.Count;
                this.item.Count -= maxAdd;
                itemTarget.Count += maxAdd;
            }
            else
            {
                itemTarget.Count += this.item.Count;
                SetEmptySlot();
            }
        }
    }

    public void DropItem()
    {
        if (!InventoryController.Instance.dropContainerOpened && !empty)
        {
            Info_Panel Info_PanelInstance = Instantiate(InventoryController.Instance.info_Panel, FindObjectOfType<Canvas>().transform);
            Info_PanelInstance.gameObject.SetActive(true);
            Info_PanelInstance.text.text = "Вы действительно хотите выбросить этот предмет?";
            DropContainerUI q = Instantiate(Info_PanelInstance.dropContainer);
            q.transform.SetParent(Info_PanelInstance.content);
            q.DropInfoPanel = Info_PanelInstance.DropInfoPanel;
            q.transform.Find("Drop_Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/Icons/Items/" + item.ObjectSlug);
            q.transform.Find("Drop_Name").GetComponent<Text>().text = item.ItemName;
            q.item = item;
            item.busy = true;
            Info_PanelInstance.acceptBtn.onClick.AddListener(() => OnDropAccept(Info_PanelInstance, item));
            Info_PanelInstance.declineBtn.onClick.AddListener(() => OnDropDecline(Info_PanelInstance, item));
            InventoryController.Instance.dropContainerOpened = true;
        }
    }

    private void OnDropAccept(Info_Panel info_Panel, Item item)
    {
        item.busy = false;
        InventoryController.Instance.dropContainerOpened = false;
        SetEmptySlot();
        Destroy(info_Panel.gameObject);
    }

    private void OnDropDecline(Info_Panel info_Panel, Item item)
    {
        item.busy = false;
        InventoryController.Instance.dropContainerOpened = false;
        Destroy(info_Panel.gameObject);
    }
}
