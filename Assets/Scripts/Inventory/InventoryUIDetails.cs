﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUIDetails : MonoBehaviour {

    Text itemNameText, itemDescriptionText;
    public Text statText;

    private void Start()
    {
        itemNameText = transform.Find("Item_Name").GetComponent<Text>();
        itemDescriptionText = transform.Find("Item_Description").GetComponent<Text>();
        gameObject.SetActive(false);
    }

    public void SetItem(Item item, Button selectedButton)
    {
        this.transform.SetAsLastSibling();
        gameObject.SetActive(true);
        gameObject.transform.position = Input.mousePosition;
        gameObject.transform.position = new Vector2(gameObject.transform.position.x + 150, gameObject.transform.position.y);
        statText.text = "";
        if (item.Stats != null)
        {
            foreach (BaseStat stat in item.Stats)
            {
                if (stat.BaseValue!=0)
                    statText.text += stat.StatName + ": " + stat.BaseValue + "\n";
            }
        }
        itemNameText.text= item.ItemName;
        itemDescriptionText.text = item.Description;
    }
}
