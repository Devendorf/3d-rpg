﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item: ICloneable
{
    public string ItemName;
    public enum ItemTypes { Clothing, Consumable }
    public enum WeaponTypes { Melee, Range }
    public enum IncreaseTypes { Health }
    
    public List<BaseStat> Stats;
    public string ObjectSlug;
    public string Description;
    public ItemTypes ItemType;
    public IncreaseTypes IncreaseType;
    public WeaponTypes WeaponType;
    public ClothSlotScript.SlotTypes SlotType;
    //public bool ItemModifier;
    public bool QuestItem;
    public int RestoreCount;
    public int Count;
    public int maxStackCount;
    [HideInInspector]
    public bool busy;

    public object Clone()
    {
        return this.MemberwiseClone();
    }

    /*public Item(List<BaseStat> _Stats, string _ObjectSlug)
    {

        this.Stats = _Stats;
        this.ObjectSlug = _ObjectSlug;
    }*/
    //[Newtonsoft.Json.JsonConstructor]
    public Item(List<BaseStat> _Stats, string _ObjectSlug, string _Description, ItemTypes _ItemType, WeaponTypes _WeaponType, string _ItemName, bool _ItemModifier, int _maxStackCount)
    {
        this.Stats = _Stats;
        this.ObjectSlug = _ObjectSlug;
        this.Description = _Description;
        this.ItemType = _ItemType;
        this.WeaponType = _WeaponType;
        this.ItemName = _ItemName;
        this.Count = 1;
        //this.ItemModifier = _ItemModifier;
    }

    public Item()
    {
        this.Stats = new List<BaseStat>() {
            new BaseStat(BaseStat.BaseStatType.Strength, 0),
            new BaseStat(BaseStat.BaseStatType.Agility, 0),
            new BaseStat(BaseStat.BaseStatType.Endurance, 0),
            new BaseStat(BaseStat.BaseStatType.Min_Damage, 0),
            new BaseStat(BaseStat.BaseStatType.Max_Damage, 0),
            new BaseStat(BaseStat.BaseStatType.Armor, 0)};
        this.ObjectSlug = "";
        this.ItemName = "";
        this.maxStackCount = 1;
        this.Count = 1;
    }

    //[Newtonsoft.Json.JsonConstructor]
    public Item(string _ObjectSlug, string _Description, ItemTypes _ItemType, string _ItemName, bool _ItemModifier, IncreaseTypes _IncreaseType, int _RestoreCount, int _maxStackCount)
    {
        this.ObjectSlug = _ObjectSlug;
        this.Description = _Description;
        this.ItemType = _ItemType;
        this.ItemName = _ItemName;
        //this.ItemModifier = _ItemModifier;
        this.IncreaseType = _IncreaseType;
        this.RestoreCount = _RestoreCount;
        this.Count = 1;
    }
}
