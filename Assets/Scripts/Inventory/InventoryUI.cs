﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InventoryUI : MonoBehaviour {
    public RectTransform inventoryPanel;
    public RectTransform scrollViewContent;
    public RectTransform inventoryPanelDetails;
    [SerializeField] private InputManager inputManager;
    List<InventoryUIItem> ItemContainers { get; set; }
    Item CurrentSelectedItem { get; set; }

	// Use this for initialization
	void Start () {
        //ItemContainer = Resources.Load<InventoryUIItem>("UI/Inventory/Item_Container");
        UIEventHandler.OnItemAddedToInventory += ItemAdded;
        //UIEventHandler.OnItemPickUp += ItemAdded;
        inventoryPanel.gameObject.SetActive(false);
        UIEventHandler.OnInventorySlotClear += SetEmptySlot;
        UIEventHandler.OnInventorySlotCountChanged += SlotRecount;
    }

    void SlotRecount(GameObject obj)
    {
        InventoryUIItem item = obj.GetComponent<InventoryUIItem>();
        if (item.hotBarSlot != null)
            item.hotBarSlot.UpdateVisualCount();
        item.UpdateVisualCount();
    }

    private void Update()
    {
        if (Input.GetKeyDown(inputManager.InventoryKeyCode))
        {
            inventoryPanel.gameObject.SetActive(!inventoryPanel.gameObject.activeSelf);
            if (inventoryPanel.gameObject.activeSelf)
            {
                UIEventHandler.CloseInventory();
            }
        }
    }

    public void ItemAdded(Item item)
    {
        bool needToAdd = true;
        ItemContainers = inventoryPanel.GetComponentsInChildren<InventoryUIItem>().ToList();
        foreach (InventoryUIItem slot in ItemContainers)
        {
            if (item.Count > 0)
            {
                if (slot.item != null)
                {
                    if (slot.item.ObjectSlug == item.ObjectSlug)
                    {
                        if (slot.item.Count < slot.item.maxStackCount)
                        {
                            if (slot.item.Count + item.Count <= slot.item.maxStackCount)
                            {
                                slot.item.Count += item.Count;
                                slot.UpdateVisualCount();
                                needToAdd = false;
                            }
                            else
                            {
                                int maxAdd = slot.item.maxStackCount - slot.item.Count;
                                slot.item.Count += maxAdd;
                                item.Count -= maxAdd;
                                slot.UpdateVisualCount();
                            }
                        }
                    }
                }
            }
        }
        if (needToAdd)
        {
            InventoryUIItem emptyItem = ItemContainers.Find(x => x.empty == true);
            if (emptyItem == null)
                Debug.Log("Full inv!");
            else
            {
                emptyItem.SetItem(item);
                emptyItem.transform.SetParent(scrollViewContent);
            }
        }
    }


    //поднятие с выкидыванием
    /*public void ItemAdded(Item item, PickupItem obj)
    {
        ItemContainers = inventoryPanel.GetComponentsInChildren<InventoryUIItem>().ToList();
        InventoryUIItem emptyItem = ItemContainers.Find(x => x.empty == true);
        if (emptyItem == null)
        {
            //PickupItem pickUpItem=(PickupItem)Resources.Load("Item");
            PickupItem instance = Instantiate(obj, obj.transform.position, Quaternion.identity, obj.transform.parent);
            instance.ItemDrop = item;
            Debug.Log("Full inv!");
        }
        else
        {
            emptyItem.SetItem(item);
            emptyItem.transform.SetParent(scrollViewContent);
        }
    }*/

    public void ItemAdded(Item item, int slotIndex)
    {
        ItemContainers = inventoryPanel.GetComponentsInChildren<InventoryUIItem>().ToList();
        InventoryUIItem emptyItem = ItemContainers[slotIndex];
        emptyItem.SetItem(item);
        emptyItem.transform.SetParent(scrollViewContent);
    }

    void SetEmptySlot(GameObject obj)
    {
        InventoryUIItem item = obj.GetComponent<InventoryUIItem>();
        item.SetEmptySlot();
    }

}
