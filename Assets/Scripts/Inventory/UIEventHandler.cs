﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEventHandler : MonoBehaviour {

    public delegate void ItemEventHandler(Item item);
    public static event ItemEventHandler OnItemAddedToInventory;
    public static event ItemEventHandler OnItemEquipped;

    /*
    public delegate void ItemPickUpHandler(Item item, PickupItem obj);
    public static event ItemPickUpHandler OnItemPickUp;
    */

    public delegate void StatPointsEventHandler(int countOfPoints);
    public static event StatPointsEventHandler OnPointsUpdate;

    public delegate void InventoryEventHandler();
    public static event InventoryEventHandler OnInventoryClosed;
    public static event InventoryEventHandler OnInventoryDblClickItem;

    public delegate void ItemInInventoryEventHandler(GameObject obj);
    public static event ItemInInventoryEventHandler OnInventorySlotClear;
    public static event ItemInInventoryEventHandler OnInventorySlotCountChanged;

    public delegate void GoalEventHandler();
    public static event GoalEventHandler OnGoalChanged;

    public static void ClearSlot(GameObject obj)
    {
        OnInventorySlotClear(obj);
    }

    public static void SlotRecount(GameObject obj)
    {
        OnInventorySlotCountChanged(obj);
    }

    public static void UseItem()
    {
        OnInventoryDblClickItem();
    }

    public static void PointsCountChanged(int countOfPoints)
    {
        OnPointsUpdate(countOfPoints);
    }

    public static void CloseInventory()
    {
        OnInventoryClosed();
    }

    public static void GoalAmountChange()
    {
        OnGoalChanged();
    }

    public static void ItemAddedToInventory(Item item)
    {
        OnItemAddedToInventory(item);
    }

    /*public static void ItemPickedUp(Item item, PickupItem obj)
    {
        OnItemPickUp(item, obj);
    }*/  

    public static void ItemEquipped(Item item)
    {
        OnItemEquipped(item);
    }
}
