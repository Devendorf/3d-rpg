﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : ScriptableObject
{
    
    public static ItemDatabase Instance { get; set; }
    [SerializeField]
    public List<Item> Items=new List<Item>();

    // Use this for initialization
    void Awake()
    {
        //if (Instance != null && Instance != this)
           // Destroy(gameObject);
       // else
        Instance = this;
    }

    public Item GetItem(string itemSlug)
    { 
        foreach (Item item in Items)
        {
            if (item.ObjectSlug == itemSlug)
                return (Item)item.Clone();
        }
        Debug.LogWarning("Couldn't find item: " + itemSlug);
        return null;
    }

    public Item GetItem(int index)
    {
        try
        {
            return (Item)Items[index].Clone();
        }
        catch
        {
            Debug.LogWarning("Couldn't find item with index: " + index);
            return null;
        }
    }
}
