﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour {
    public static InventoryController Instance { get; set; }
    public Info_Panel info_Panel;
    public bool dropContainerOpened;
    public PlayerWeaponController playerWeaponController;
    public ConsumableController consumableController;
    public InventoryUIDetails inventoryDetailsPanel;
    //public List<Item> playerItems = new List<Item>();
    public ItemDatabase itemDatabase;

    void Start()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            Instance = this;
        playerWeaponController = GetComponent<PlayerWeaponController>();
        consumableController = GetComponent<ConsumableController>();
        GiveItem("sword");
        GiveItem("potion_log",10);
        GiveItem("q_log");
        GiveItem("testhead");
        GiveItem("testhed");
    }

    public void GiveItem(string itemSlug, int count=1)
    {
        Item item = itemDatabase.GetItem(itemSlug);
        if (item != null)
        {
            if (count > item.maxStackCount)
            {
                count -= item.maxStackCount;
                item.Count = item.maxStackCount;
                UIEventHandler.ItemAddedToInventory(item);
                GiveItem(itemSlug, count);

            }
            else
            {
                item.Count = count;
                UIEventHandler.ItemAddedToInventory(item);
            }
        }
    }

    public void GiveItem(int index, int count=1)
    {
        Item item = itemDatabase.GetItem(index);
        if (item != null)
        {
            if (count > item.maxStackCount)
            {
                count -= item.maxStackCount;
                item.Count = item.maxStackCount;
                UIEventHandler.ItemAddedToInventory(item);
                GiveItem(index, count);
            }
            else
            {
                item.Count = count;
                UIEventHandler.ItemAddedToInventory(item);
            }
        }
    }

    /* public void PickUpItem(Item item, PickupItem obj)
    {
         playerItems.Add(item);
         UIEventHandler.ItemPickedUp(item,obj);
     }*/

    public void GiveItem(Item item)
    {
        //foreach (Item temp in playerItems)
       // {
       //     if (temp.ObjectSlug)
       // }
        //playerItems.Add(item);
        UIEventHandler.ItemAddedToInventory(item);
    }

    public void SetItemDetails(Item item,Button selectedButton)
    {
        inventoryDetailsPanel.SetItem(item, selectedButton);
    }

    public void ClosePanelDetails()
    {
        inventoryDetailsPanel.gameObject.SetActive(false);
    }

    public void EquipItem(Item itemToEquip)
    {
        playerWeaponController.EquipWeapon(itemToEquip);
    }

    public void ConsumeItem(Item itemToConsume)
    {
        consumableController.ConsumeItem(itemToConsume);
    }
}
