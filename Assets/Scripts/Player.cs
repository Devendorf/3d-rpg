﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public CharacterStats characterStats;
    public int freeStatsPoints;
    public int currentHealth;
    public int regen { get { return (int)(maxHealth * 0.05); } }
    public int maxHealth { get { return characterStats.GetStat(BaseStat.BaseStatType.Endurance).GetFinalValue() * 10; } }
    public PlayerLevel PlayerLevel { get; set; }
    public bool died;
    private bool healBlocked;
    public Vector3 respawn;
    [SerializeField] private RectTransform deadPanel;
    private Button btnRespawn;
    bool regenstarted;
    public bool busy;

    private void Start()
    {
        regenstarted = false;
        healBlocked = false;
        PlayerLevel = GetComponent<PlayerLevel>();
        characterStats = new CharacterStats(4, 4, 4, 2, 3, 5);
        this.currentHealth = this.maxHealth;
        died = false;
        freeStatsPoints = 0;
        btnRespawn = deadPanel.GetComponentInChildren<Button>();
        btnRespawn.onClick.AddListener(Respawn);
        PlayerEventHandler.OnPlayerLevelUp += EventAtLvlUP;
        PlayerEventHandler.OnPlayerHealhChanged += HP_recalculation;
        PlayerEventHandler.OnPlayerHealthRestored += RegenHP;
    }

    void HP_recalculation()
    {
        if (currentHealth > maxHealth)
        {
            SetFullHP();
        }
    }

    void EventAtLvlUP()
    {
        SetFullHP();
        AddFreeStatPoints(3);
    }

    void AddFreeStatPoints(int count)
    {
        freeStatsPoints += count;
        UIEventHandler.PointsCountChanged(freeStatsPoints);
    }

    public void TakeDamage(int amount)
    {
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            Die();
        }
        //if (GetComponent<CharacterPanelControl>().characterPanel.gameObject.activeSelf)
            PlayerEventHandler.HealthChanged();
    }

    public void RestoreHealth(int amount)
    {
        if (!healBlocked)
        {
            if ((this.maxHealth - this.currentHealth) <= amount)
            {
                SetFullHP();
            }
            else
            {
                this.currentHealth += amount;
            }
            PlayerEventHandler.HealthChanged();
        }
    }

    private void Die()
    {
        currentHealth = 0;
        Debug.Log("Player dead!!!!!!");
        died = true;
        healBlocked = true;
        deadPanel.gameObject.SetActive(true);
    }

    private void SetFullHP()
    {
        this.currentHealth = this.maxHealth;
        PlayerEventHandler.HealthChanged();
    }

    private void Respawn()
    {
        //gameObject.transform.position = respawn;
        PortalToAnyScene.Instance.TeleportToHome();
        SetFullHP();
        died = false;
        healBlocked = false;
        deadPanel.gameObject.SetActive(false);
    }

    void RegenHP()
    {
        if (!regenstarted)
            StartCoroutine(Regeneration());
    }

    IEnumerator Regeneration()
    {
        regenstarted = true;
        while (currentHealth < maxHealth)
        {
            RestoreHealth(regen);
            yield return new WaitForSecondsRealtime(1.5f);
        }
        regenstarted = false;
        yield  break;
    }
}
