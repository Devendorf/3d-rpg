﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HotBarSlot : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public InventoryUIItem inventoryUIItem;
    public BaseSkill skill;
    public Item item;
    public Text itemText;
    public Image itemImage;
    public Button btn;
    //private bool test,test_1, test_2;
    private float lastClick;
    public bool empty;
    public Text visualCount;
    [SerializeField]private bool ready;

    public int slotNumber;

    private void Start()
    {
        visualCount.gameObject.SetActive(false);
        empty = true;
        this.btn = this.transform.GetComponent<Button>();
        btn.onClick.AddListener(OnClick);
    }
    
    public void SetItem()
    {
        empty = false;
        SetupItemValues();
    }
    
    void SetupItemValues()
    {
        this.btn = this.transform.GetComponent<Button>();
        btn.onClick.AddListener(OnClick);
        //this.transform.Find("Item_Name").GetComponent<Text>().text = item.ItemName;
        this.transform.Find("Item_Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/Icons/Items/" + inventoryUIItem.item.ObjectSlug);
        UpdateVisualCount();
    }

    private void Update()
    {
        if (ready)
        {
            if (Input.GetKeyDown(InputManager.Instance.hotbarBinds[slotNumber]))
            {
                if (inventoryUIItem!=null)
                    inventoryUIItem.OnItemInteract();
                if (skill != null)
                {
                    FightsEventHandler.UseSkill(skill);
                }
            }
        }
       /* if (test && !test_1 && !test_2 && item != null)
        {
            InventoryController.Instance.SetItemDetails(item, GetComponent<Button>());
            test_2 = true;
        }*/
    }

    public void UpdateVisualCount()
    {
        if (inventoryUIItem != null) {
            if (inventoryUIItem.item != null)
            {
                if (inventoryUIItem.item.Count > 1)
                {
                    this.visualCount.gameObject.SetActive(true);
                    this.visualCount.text = inventoryUIItem.item.Count.ToString();
                }
                else
                {
                    this.visualCount.gameObject.SetActive(false);
                }
            }
            else
                this.visualCount.gameObject.SetActive(false);
        }
        else
            this.visualCount.gameObject.SetActive(false);
    }
    
    public void SetEmptySlot()
    {
        this.transform.Find("Item_Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/Icons/Items/EmptySlot");
        empty = true;
        item = null;
        inventoryUIItem.hotBarSlot = null;
        inventoryUIItem = null;
        UpdateVisualCount();
        btn.onClick.RemoveAllListeners();
        btn = null;
        
    }

    public void OnClick()
    {
        if (inventoryUIItem != null)
            inventoryUIItem.OnItemInteract();
    }


    /*public void OnSelectItemButton()
    {
        InventoryController.Instance.SetItemDetails(item, GetComponent<Button>());
    }*/
    
   /* public void OnPointerEnter(PointerEventData eventData)
    {
        if (!empty)
        {
            InventoryController.Instance.SetNotFullItemDetails(item, GetComponent<Button>());
            SetTest();
            //StartCoroutine(Example());
            test = true;
        }
    }

    IEnumerator Example()
    {
        yield return new WaitForSecondsRealtime(1f);
        //test = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        test_1 = true;
        //StopCoroutine(Example());
        InventoryController.Instance.ClosePanelDetails();
    }

    void SetTest()
    {
        test = false;
        test_1 = false;
        test_2 = false;
        InventoryController.Instance.ClosePanelDetails();
    }*/


    //Перетаскивание объекта


    private Transform oldParent;
    private Vector3 previous, real,start;
    private string objectName;

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!empty)
        {
            start = itemImage.transform.position;
            oldParent = itemImage.gameObject.transform.parent;
            itemImage.transform.SetParent(FindObjectOfType<Canvas>().transform);
            objectName = eventData.pointerCurrentRaycast.gameObject.name;
            previous = Input.mousePosition;
            inventoryUIItem.item.busy = true;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (objectName == this.gameObject.name&& item!=null&& !empty)
        {
            real = Input.mousePosition - previous;
            itemImage.gameObject.transform.position = itemImage.transform.position + real;
            previous = Input.mousePosition;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!empty)
        {
            //Debug.Log(eventData.pointerCurrentRaycast.gameObject.name);
            //if (eventData.pointerCurrentRaycast.gameObject.GetComponent<InventoryUIItem>())
            itemImage.gameObject.transform.SetParent(oldParent);
            itemImage.transform.position = start;
            inventoryUIItem.item.busy = false;
            if (eventData.pointerCurrentRaycast.gameObject == null)
            {
                SetEmptySlot();
            }
            else
            if (this != eventData.pointerCurrentRaycast.gameObject.GetComponent<HotBarSlot>() && eventData.pointerCurrentRaycast.gameObject.GetComponent<HotBarSlot>() != null)
                SwapItems(eventData.pointerCurrentRaycast.gameObject.GetComponent<HotBarSlot>());
            else
            {
                GameObject inv = GameObject.Find("HotBar");
                if (eventData.pointerCurrentRaycast.gameObject != inv && eventData.pointerCurrentRaycast.gameObject.transform.parent.gameObject != inv)
                    SetEmptySlot();
            }
            //else
            
        }
    }

    public InventoryUIItem GetLinkToItem()
    {
        //return this;
        return null;
    }

    void SwapItems(HotBarSlot target)
    {
        if (target.empty)
        {
            inventoryUIItem.SetHotBarSlot(target, inventoryUIItem, false);
            SetEmptySlot();
        }
        else
        {
            InventoryUIItem tempInventoryUIItem = target.inventoryUIItem;
            HotBarSlot tempHotBarSlot = target;
            this.inventoryUIItem.SetHotBarSlot(tempHotBarSlot, inventoryUIItem, false);
            tempInventoryUIItem.SetHotBarSlot(this, tempInventoryUIItem, false);
        }
    }

}
