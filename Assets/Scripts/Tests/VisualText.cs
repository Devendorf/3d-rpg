﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualText: MonoBehaviour {

    public virtual void Start()
    {
        CreateVisualName(this.transform);
    }
    public virtual void CreateVisualName(Transform target)
    {
        ObjectLabel objectLabel =((GameObject)(Instantiate(Resources.Load("UIName"),target))).GetComponent<ObjectLabel>();
        objectLabel.target = target;
        objectLabel.Name =this.GetComponent<Interactable>().Name;
    }
}
