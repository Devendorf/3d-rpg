﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class Slime : BaseEnemy
{
    override public void Start()
    {
        DropTable = new DropTable();
        DropTable.loot = new List<LootDrop>
        {
            new LootDrop("sword",25),
            new LootDrop("potion_log",25)
        };
        ID = 0;
        Experience = 30;        
        characterStats = new CharacterStats(50, 10, 10, 4, 1, 2);
        base.Start();
    }
}
