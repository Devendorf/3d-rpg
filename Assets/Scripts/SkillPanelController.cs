﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillPanelController : MonoBehaviour {
    public RectTransform skillPanel;
    [SerializeField] private InputManager inputManager;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(inputManager.TalantsKeyKode))
        {
            skillPanel.gameObject.SetActive(!skillPanel.gameObject.activeSelf);
        }
	}
}
