﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseStat: ICloneable {
    public enum BaseStatType { Strength, Agility, Endurance, Min_Damage, Max_Damage, Armor, None
    }
    [HideInInspector]
    public static Dictionary<BaseStatType, string> TypesName = new Dictionary<BaseStatType, string>
    {
        { BaseStatType.Agility,"Ловкость" },
        { BaseStatType.Armor,"Защита" },
        { BaseStatType.Endurance,"Выносливость" },
        { BaseStatType.Strength,"Сила" },
        { BaseStatType.Min_Damage,"Минимальный урон" },
        { BaseStatType.Max_Damage,"Максимальный урон" }
    };
    public BaseStatType StatType;
    public List<StatBonus> BaseAdditives { get; set; }
    public int BaseValue;
    public int PointsToStat;
    public string StatName { get { return TypesName[StatType]; } }
    public string StatDescription { get; set; }
    public int FinalValue { get; set; }
    BaseStatType dependedOnStat=BaseStatType.None;
    int dependedIndex = -1;

    public object Clone()
    {
        return this.MemberwiseClone();
    }

    public BaseStat(int baseValue, string statName, string statDescription)
    {
        this.BaseAdditives = new List<StatBonus>();
        this.BaseValue = baseValue;
        //this.StatName = statName;
        this.StatDescription = statDescription;
    }
    
    public BaseStat(BaseStatType statType, int baseValue, string statName)
    {
        this.BaseAdditives = new List<StatBonus>();
        this.StatType = statType;
        this.BaseValue = baseValue;
        //this.StatName = statName;
    }

    public BaseStat(BaseStatType statType, int baseValue)
    {
        this.BaseAdditives = new List<StatBonus>();
        this.StatType = statType;
        this.BaseValue = baseValue;
    }

    public BaseStat(BaseStatType statType, int baseValue, BaseStatType _dependedOnStat = BaseStatType.None, int _dependedIndex = -1)
    {
        this.BaseAdditives = new List<StatBonus>();
        this.StatType = statType;
        this.BaseValue = baseValue;

        dependedOnStat = _dependedOnStat;
        dependedIndex = _dependedIndex;
    }

    public void AddStatBonus(StatBonus statBonus)
    {
        this.BaseAdditives.Add(statBonus);
        bool needHealthUPd = false;
        if (this.StatType == BaseStatType.Endurance)
            //PlayerEventHandler.HealthChanged();3
            needHealthUPd = true;
        PlayerEventHandler.StatsChanged(needHealthUPd);
    }

    public void RemoveStatBonus(StatBonus staBonus)
    {
        this.BaseAdditives.Remove(BaseAdditives.Find(x => x.BonusValue == staBonus.BonusValue));
        PlayerEventHandler.StatsChanged(true);
    }

    public void RemoveStatBonus(StatBonus staBonus, bool needHealthUpdate)
    {
        this.BaseAdditives.Remove(BaseAdditives.Find(x => x.BonusValue == staBonus.BonusValue));
        PlayerEventHandler.StatsChanged(needHealthUpdate);
    }

    public int GetFinalValue(CharacterStats characterStats=null)
    {
        this.FinalValue = 0;
        this.BaseAdditives.ForEach(x => this.FinalValue +=x.BonusValue);
        FinalValue += GetValueWithPoints();

        if (dependedOnStat!=BaseStatType.None)
        {
            FinalValue += characterStats.stats.Find(x => x.StatType == dependedOnStat).GetFinalValue() * dependedIndex;
        }

        return FinalValue;
    }

    public int GetValueWithPoints()
    {
        int value = 0;
        value = BaseValue + PointsToStat;
        return value;
    }

    public int GetBonuses()
    {
        int value = 0;
        this.BaseAdditives.ForEach(x => value += x.BonusValue);
        return value;
    }
}