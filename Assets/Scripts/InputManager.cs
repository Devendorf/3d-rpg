﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {
    public static InputManager Instance { get; set; }

    public KeyCode InventoryKeyCode = KeyCode.B;
    public KeyCode CharacterPanelKeyCode = KeyCode.C;
    public KeyCode TalantsKeyKode = KeyCode.T;
    public KeyCode QuestsKeyKode = KeyCode.Q;
    public KeyCode PerfomAttackKeyKode = KeyCode.X;

    public KeyCode SlotSeparation = KeyCode.LeftControl;

    public KeyCode FirstHotbarSlotKey = KeyCode.Alpha1;
    public KeyCode SecondHotbarSlotKey = KeyCode.Alpha2;
    public KeyCode ThirdHotbarSlotKey = KeyCode.Alpha3;
    public KeyCode ForthHotbarSlotKey = KeyCode.Alpha4;
    public KeyCode FifthHotbarSlotKey = KeyCode.Alpha5;
    public KeyCode SixthHotbarSlotKey = KeyCode.Alpha6;
    public KeyCode SeventhHotbarSlotKey = KeyCode.Alpha7;
    public KeyCode EighthHotbarSlotKey = KeyCode.Alpha8;
    public KeyCode NinthHotbarSlotKey = KeyCode.Alpha9;

    public Dictionary<int, KeyCode> hotbarBinds= new Dictionary<int, KeyCode>();

    private void Start()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            Instance = this;

        hotbarBinds.Add(1, FirstHotbarSlotKey);
        hotbarBinds.Add(2, SecondHotbarSlotKey);
        hotbarBinds.Add(3, ThirdHotbarSlotKey);
        hotbarBinds.Add(4, ForthHotbarSlotKey);
        hotbarBinds.Add(5, FifthHotbarSlotKey);
        hotbarBinds.Add(6, SixthHotbarSlotKey);
        hotbarBinds.Add(7, SeventhHotbarSlotKey);
        hotbarBinds.Add(8, EighthHotbarSlotKey);
        hotbarBinds.Add(9, NinthHotbarSlotKey);
    }
}
