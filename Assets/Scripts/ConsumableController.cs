﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumableController : MonoBehaviour {
    //CharacterStats stats;
    Player player;

	// Use this for initialization
	void Start () {
        //stats = GetComponent<Player>().characterStats;
        player= GetComponent<Player>();
    }

    public void ConsumeItem(Item item)
    {
        GameObject itemToSpawn = Instantiate(Resources.Load<GameObject>("Consumables/" + item.ObjectSlug));
        //if (item.ItemModifier)
       // {
       //     itemToSpawn.GetComponent<IConsumable>().Consume(stats);
       // }
       // else
            itemToSpawn.GetComponent<IConsumable>().Consume(player, item);
    }
}
