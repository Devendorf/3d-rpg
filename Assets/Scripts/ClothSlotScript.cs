﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClothSlotScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject EquippedWeapon { get; set; }
    [SerializeField] private Player player;
    [SerializeField] private Button weaponButton;
    [SerializeField] private Sprite defaultWeaponSpite;
    private PlayerWeaponController playerWeaponController;
    [SerializeField] private Text weaponStatPrefab;
    [SerializeField] private Transform weaponStatPanel;
    [SerializeField] private Image weaponIcon;
    private List<Text> weaponStatTexts = new List<Text>();
    private float lastClick;
    [HideInInspector] public IWeapon weapon;
    [System.NonSerialized] public Item item=null;

    public enum SlotTypes { Head, Body, Legs, Boots, Hands, First_Weapon, Second_Weapon, Necklace, Ring, Trinket }
    public SlotTypes slotType;

    void Start()
    {
        playerWeaponController = player.GetComponent<PlayerWeaponController>();
        UIEventHandler.OnItemEquipped += UpdateEqippedWeapon;
        weaponButton.onClick.AddListener(OnClick);
    }

    void UpdateEqippedWeapon(Item item)
    {
        if (item.SlotType == this.slotType)
        {
            this.item = item;
            for (int i = 0; i < weaponStatTexts.Count; i++)
            {
                Destroy(weaponStatTexts[i].gameObject);
            }
            weaponStatTexts.Clear();
            weaponIcon.sprite = Resources.Load<Sprite>("UI/Icons/Items/" + item.ObjectSlug);
            if (this.slotType == SlotTypes.First_Weapon)
            {
                weapon = EquippedWeapon.GetComponent<IWeapon>();
            }
            /*weaponNameText.text = item.ItemName;
            for (int i = 0; i < item.Stats.Count; i++)
            {
                weaponStatTexts.Add(Instantiate(weaponStatPrefab));
                weaponStatTexts[i].transform.SetParent(weaponStatPanel);
                //weaponStatTexts[i].text = item.Stats[i].StatName + ": " + item.Stats[i].GetCalculatedStatValue().ToString();
            }*/
        }
    }

    public void OnClick()
    {
        if (Time.time - lastClick < 0.3f)
        {
            UnequipWeapon();
        }
        lastClick = Time.time;
    }

    public void UnequipWeapon()
    {
        playerWeaponController.UnequipWeapon(this);
        test_1 = true;
        SlotInfoPanel.gameObject.SetActive(false);
        this.item = null;
        weaponIcon.sprite = defaultWeaponSpite;
        
    }

    public InventoryUIDetails SlotInfoPanel;
    private bool test, test_1, test_2;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (item != null)
        {
            SetTest();
            test = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        test_1 = true;
        SlotInfoPanel.gameObject.SetActive(false);
    }

    void SetTest()
    {
        test = false;
        test_1 = false;
        test_2 = false;
        SlotInfoPanel.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (test && !test_1 && !test_2 && item != null)
        {
            SlotInfoPanel.SetItem(item, GetComponent<Button>());
            test_2 = true;
        }
    }
}
