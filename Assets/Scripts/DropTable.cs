﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropTable{
    public List<LootDrop> loot;

    public List<Item> GetDrop()
    {
        List<Item> value=new List<Item>();
        foreach(LootDrop drop in loot)
        {
            if (drop.ChanceType == LootDrop.NumbTypes.Int)
            {
                if (MyRandom.Get_Result(drop.IntChance))
                {
                    value.Add(InventoryController.Instance.itemDatabase.GetItem(drop.ItemSlug));
                }
            }
            else if (drop.ChanceType ==LootDrop.NumbTypes.Double)
            {
                if (MyRandom.Get_Result(drop.DoubleChance))
                {
                    value.Add(InventoryController.Instance.itemDatabase.GetItem(drop.ItemSlug));
                }
            }
        }
        if (value.Count < 1)
            return null;
        else
            return value;
    }
}

public class LootDrop
{
    public enum NumbTypes {Int, Double}
    public NumbTypes ChanceType { get; set; }
    public string ItemSlug{ get; set; }
    public int IntChance { get; set; }
    public double DoubleChance { get; set; }

    public LootDrop(string itemSlug, int chance)
    {
        this.ItemSlug = itemSlug;
        this.IntChance = chance;
        ChanceType = NumbTypes.Int;
    }
    
    public LootDrop(string itemSlug, double chance)
    {
        this.ItemSlug = itemSlug;
        this.DoubleChance = chance;
        ChanceType = NumbTypes.Double;
    }
}