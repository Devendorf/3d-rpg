﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IConsumable {

    void Consume();
    void Consume(CharacterStats stats);
    void Consume(Player player, Item item);
}
