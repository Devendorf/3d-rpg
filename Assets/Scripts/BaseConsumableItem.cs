﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseConsumableItem : MonoBehaviour,IConsumable 
{
    public void Consume()
    {

    }

    public void Consume(CharacterStats stats)
    {

    }

    public void Consume(Player player, Item item)
    {
        player.RestoreHealth(item.RestoreCount);
        Destroy(gameObject);
    }
}
