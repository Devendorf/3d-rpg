﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP_Bar : MonoBehaviour {
    [SerializeField] private Image healthFill;
    [SerializeField] private Text healthValue;
    [SerializeField] private Player player;

    private void Start()
    {
        UpdateHealth();
        PlayerEventHandler.OnPlayerHealhChanged += UpdateHealth;
    }

    public void UpdateHealth()
    {
        this.healthFill.fillAmount = (float)player.currentHealth / (float)player.maxHealth;
        this.healthValue.text = player.currentHealth.ToString() + "/" + player.maxHealth.ToString();
    }
}
