﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeapon {

    List<BaseStat> Stats { get; set; }
    int CurrentDamage { get; set; }
    Item.WeaponTypes WeaponType { get; set; }
    float AttackDistance { get; set; }
    void PerformAttack(int damage, bool _isPoison=false);
    void PerformSpecialAttack(int damage, int poisdmg, bool _isPoison, float time);
}
