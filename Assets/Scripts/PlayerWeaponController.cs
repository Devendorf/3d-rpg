﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerWeaponController : MonoBehaviour {

    public GameObject playerHand;
    public  GameObject playerHead;
    public GameObject EquippedWeapon { get; set; }
    Transform spawnProjectile;
    IWeapon equippedWeapon;
    CharacterStats characterStats;

    public Transform characterPanel;
    private List<ClothSlotScript> listOfSlots;
    private ClothSlotScript requiredSlot;
    private ClothSlotScript SlotWithWeapon;
    public static Dictionary<ClothSlotScript.SlotTypes, GameObject> BodyPart = new Dictionary<ClothSlotScript.SlotTypes, GameObject>();


    void Start()
    {
        BodyPart.Add(ClothSlotScript.SlotTypes.First_Weapon, playerHand);
        BodyPart.Add(ClothSlotScript.SlotTypes.Head, playerHead);
        spawnProjectile = transform.Find("ProjectileSpawn");
        characterStats = GetComponent<Player>().characterStats;
        if (GetComponent<Player>().characterStats == null) { Debug.Log("asd"); }
        listOfSlots = characterPanel.GetComponentsInChildren<ClothSlotScript>().ToList();
        foreach (ClothSlotScript slot in listOfSlots)
        {
            if (slot.slotType == ClothSlotScript.SlotTypes.First_Weapon)
                SlotWithWeapon = slot;
        }
        FightsEventHandler.OnSkillUse += PerformWeaponSpecialAttack;
    }

    public void EquipWeapon(Item itemToEquip)
    {
        foreach(ClothSlotScript slot in listOfSlots)
        {
            if (slot.slotType == itemToEquip.SlotType)
                requiredSlot = slot;
        }
        if (requiredSlot.item!= null)
        {
            UnequipWeapon(requiredSlot, false);
        }
        requiredSlot.EquippedWeapon = (GameObject)Instantiate(Resources.Load<GameObject>("Weapons/" + itemToEquip.ObjectSlug), BodyPart[itemToEquip.SlotType].transform);
        //equippedWeapon = requiredSlot.EquippedWeapon.GetComponent<IWeapon>();
        if (requiredSlot.EquippedWeapon.GetComponent<IProjectileWeapon>() != null)
            requiredSlot.EquippedWeapon.GetComponent<IProjectileWeapon>().ProjectileSpawn = spawnProjectile;
        //equippedWeapon.Stats = itemToEquip.Stats;
        requiredSlot.EquippedWeapon.transform.SetParent(BodyPart[itemToEquip.SlotType].transform, false);
        characterStats.AddStatBonus(itemToEquip.Stats);
       // equippedWeapon.WeaponType = itemToEquip.WeaponType;
        //if (equippedWeapon.WeaponType == Item.WeaponTypes.Melee)
       // {
       //     equippedWeapon.AttackDistance = requiredSlot.EquippedWeapon.transform.localScale.x * 20;
       // }
       // else
       // {
       //     equippedWeapon.AttackDistance = requiredSlot.EquippedWeapon.GetComponent<Staff>().AttackDistance;
      //  }
        UIEventHandler.ItemEquipped(itemToEquip);       
    }

    public void UnequipWeapon(ClothSlotScript slot)
    {
        if (slot.item != null)
        {
            InventoryController.Instance.GiveItem(slot.item.ObjectSlug);
            characterStats.RemoveStatBonus(slot.item.Stats);
            Destroy(BodyPart[slot.slotType].transform.GetChild(0).gameObject);
        }
    }

    public void UnequipWeapon(ClothSlotScript slot, bool needUpdateHealth)
    {
        if (slot.item != null)
        {
            InventoryController.Instance.GiveItem(slot.item.ObjectSlug);
            characterStats.RemoveStatBonus(slot.item.Stats, needUpdateHealth);
            Destroy(BodyPart[slot.slotType].transform.GetChild(0).gameObject);
        }
    }

    void Update()
    {
        if (SlotWithWeapon.item != null)
        {
            if (Input.GetKeyDown(InputManager.Instance.PerfomAttackKeyKode))
            {
                PerformWeaponAttack();
            }
        }
    }

    public void PerformWeaponAttack()
    {
        SlotWithWeapon.weapon.PerformAttack(Damage.CalculateDamage(characterStats));
    }

    public void PerformWeaponSpecialAttack(BaseSkill skill)
    {
        if (skill.ready)
        {
            SlotWithWeapon.weapon.PerformSpecialAttack(skill.GetDamage(characterStats,this), skill.damageFromPoison, skill.isPoison(), skill.poisonTime);
        }
        else
        {
            Debug.Log("CD " + Math.Round(skill.castTime, 2));
        }
    }
}
